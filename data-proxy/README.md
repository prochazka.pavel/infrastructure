# Rabin
```bash
helm upgrade -n golemio --install --values ./data-proxy/values-development-versions.yaml --values cluster_development/golemio_data-proxy-values.yaml data-proxy ./data-proxy
```

# Golem
```bash
helm upgrade -n golemio --install --values ./data-proxy/values-master-versions.yaml --values cluster_production/golemio_data-proxy-values.yaml data-proxy ./data-proxy
```
