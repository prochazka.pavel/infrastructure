{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "data-proxy.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "data-proxy.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "data-proxy.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "data-proxy.selectorLabels" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ include "data-proxy.name" . }}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "data-proxy.labels" -}}
{{ include "data-proxy.selectorLabels" . }}
helm.sh/chart: {{ include "data-proxy.chart" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- end -}}

{{/*
Common annotations
*/}}
{{- define "data-proxy.annotations" -}}
{{- end -}}

{{/*
Custom helpers
*/}}

{{- define "data-proxy.imagePullSecrets" -}}
  {{- $top := index . 0 -}}
  {{- $pullSecrets := index . 1 -}}

  {{- if (not (empty $pullSecrets)) }}
imagePullSecrets:
    {{- range $pullSecrets | uniq }}
  - name: {{ . }}
    {{- end }}
  {{- end }}
{{- end -}}

{{- define "data-proxy.imageName" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{ index $top "Values" "global" "imageRegistry" }}/{{ index $top "Values" $appSection "repository" }}{{ if index $top "Values" $appSection "branch" }}/{{ index $top "Values" $appSection "branch" }}{{- end -}}:{{ index $top "Values" $appSection "tag" }}
{{- end -}}

{{- define "data-proxy.readinessProbe" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
httpGet:
  path: /ready
  port: 9000
initialDelaySeconds: {{ index $top "Values" "global" "readinessProbe" "initialDelaySeconds" }}
periodSeconds: {{ index $top "Values" "global" "readinessProbe" "periodSeconds" }}
{{- end -}}

{{- define "data-proxy.livenessProbe" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
httpGet:
  path: /live
  port: 9000
{{- end -}}

{{- define "data-proxy.rollingUpdate" -}}
rollingUpdate:
  maxUnavailable: {{ if gt (index . "replicas" | toDecimal) 1 -}}1{{- else -}}0{{- end }}
  maxSurge: 1
{{- end -}}

{{- define "data-proxy.maxOldSpaceSize" -}}
{{- $memoryLimit := regexFind "[0-9]+" . }}
{{- $reserveCoeficient := 0.25 }}
{{- $memoryReserve := max 64 (min 512 (mulf $reserveCoeficient $memoryLimit)) }}
{{- printf "--max-old-space-size=%d" (subf $memoryLimit $memoryReserve | int) -}}
{{- end -}}
