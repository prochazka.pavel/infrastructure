```bash
helm upgrade --install --namespace golemio golemio .
```

# Rabin
```bash
helm upgrade -n golemio --install --values ./golemio/values-development-versions.yaml --values cluster_development/golemio_golemio-values.yaml golemio ./golemio
```

# Golem
```bash
helm upgrade -n golemio --install --values ./golemio/values-master-versions.yaml --values cluster_production/golemio_golemio-values.yaml golemio ./golemio
```

# Rabin - vehicle positions
```bash
helm upgrade -n vehicle-positions --install --values ./golemio/values-development-versions.yaml --values cluster_development/vehicle-positions_golemio-values.yaml --values ./golemio/values-vp-development-versions.yaml vehicle-positions ./golemio
```

# Golem - vehicle positions
```bash
helm upgrade -n vehicle-positions --install --values ./golemio/values-master-versions.yaml --values cluster_production/vehicle-positions_golemio-values.yaml --values ./golemio/values-vp-master-versions.yaml vehicle-positions ./golemio
```
curl -X POST --data-binary @tmp.txt https://v2.kubesec.io/scan[ingress-nginx-secret](..%2Fingress-nginx-secret)


# Rollback
Rollback probíhá v několika krocích.
1. PSQL migration down
```bash
export MIGRATION_DOWN_MODULE= #DOPLNIT-NÁZEV-MODULU-PRO-DOWN-MIGRACI, například pid nebo ** pro všechny

# smazani predchoziho jobu pokud existuje
# kdel jobs.batch down-migration-job

# generuje a aplikuje job s down migrací
helm template -n vehicle-positions --values ./golemio/values-vp-master-versions.yaml \
	--set golemio-database-migration.enabled=true --set golemio-database-migration.commands="{-c,npx golemio migrate-db down}" \
	--set golemio-database-migration.name=down \
	--set integration-engine.branch=master --set integration-engine.repository=code/vp/integration-engine \
	--set golemio-database-migration.migrationsDir="node_modules/@golemio/$MIGRATION_DOWN_MODULE/db/migrations/**/postgresql" \
	./golemio | \
	kubectl apply -f -
```

2. Rollback na předchozí release
```bash
# zobrazit seznam předchozích releasů
helm history vehicle-positions

# rollback na konkretní verzi <XYZ>
helm rollback vehicle-positions <XYZ>
```