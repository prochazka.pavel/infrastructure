{{ $appSection := "output-gateway-v2" -}}
{{ $appName := (index . "Values" $appSection "name") -}}
{{- if index . "Values" $appSection "enabled" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $appName }}
  labels:
    app.kubernetes.io/name: {{ $appName }}
    backstage.io/kubernetes-id: {{ $appName }}
    {{- include "golemio.labels" . | nindent 4 }}
  annotations:
    {{- include "golemio.annotations" . | nindent 4 }}
spec:
  {{- if ge (index .Values $appSection "minReplicas") (index .Values $appSection "maxReplicas") }}
  replicas: {{ index .Values $appSection "replicas" }}
  {{- end }}
  strategy: {{- include "golemio.rollingUpdate" (index . "Values" $appSection) | nindent 4 }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ $appName }}
      {{- include "golemio.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ $appName }}
        backstage.io/kubernetes-id: {{ $appName }}
        {{- include "golemio.labels" . | nindent 8 }}
      annotations:
        {{- include "golemio.annotations" . | nindent 8 }}
    spec:
      automountServiceAccountToken: false
      {{- include "golemio.imagePullSecrets" (list . .Values.global.imagePullSecrets ) | indent 6 }}
      containers:
        - name: {{ $appName }}
          image: {{ include "golemio.imageName" (list . $appSection) | quote }}
          imagePullPolicy: {{ .Values.global.pullPolicy }}
          ports:
            - containerPort: {{ index . "Values" $appSection "port" }}
            {{- if (index . "Values" $appSection "inspector" "enabled") }}
            - containerPort: {{ index . "Values" $appSection "inspector" "port" }}
            {{- end }}
          env:
            - name: APP_NAME
              value: {{ index .Values $appSection "name" }}
            - name: NODE_ENV
              value: {{ index . "Values" $appSection "nodeEnv" }}
            - name: NODE_OPTIONS
              value: {{ include "golemio.maxOldSpaceSize" (index . "Values" $appSection "resources" "limits" "memory" ) | quote}}
            {{- if (index .Values $appSection "lightship" "handlerTimeout") }}
            - name: LIGHTSHIP_HANDLER_TIMEOUT
              value: {{ index . "Values" $appSection "lightship" "handlerTimeout" | quote }}
            {{- end }}
            {{- if (index .Values $appSection "lightship" "shutdownDelay") }}
            - name: LIGHTSHIP_SHUTDOWN_DELAY
              value: {{ index . "Values" $appSection "lightship" "shutdownDelay" | quote }}
            {{- end }}
            {{- if (index .Values $appSection "lightship" "shutdownTimeout") }}
            - name: LIGHTSHIP_SHUTDOWN_TIMEOUT
              value: {{ index . "Values" $appSection "lightship" "shutdownTimeout" | quote }}
            {{- end }}
            - name: ROARR_LOG
              value: "false"
            - name: LOG_LEVEL
              value: {{ index .Values $appSection "logLevel" | quote }}
            - name: DEBUG
              value: {{ index .Values $appSection "debugMode" }}
            - name: PORT
              value: {{ index .Values $appSection "port" | quote }}
            - name: API_URL_PREFIX
              value: {{ index . "Values" $appSection "apiUrlPrefix" | quote }}
            - name: POSTGRES_CONN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: outputGatewayPostgresConn
            - name: POSTGRES_POOL_MIN_CONNECTIONS
              value: {{ index .Values $appSection "postgres" "minConnections" | quote }}
            - name: POSTGRES_POOL_MAX_CONNECTIONS
              value: {{ index .Values $appSection "postgres" "maxConnections" | quote }}
            - name: POSTGRES_POOL_IDLE_TIMEOUT
              value: {{ index .Values $appSection "postgres" "idleTimeout" | quote }}
            - name: REDIS_ENABLE
              value: {{ index .Values.redis.enabled | quote }}
            - name: REDIS_CONN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: og-redis-conn
            - name: REDIS_DEFAULT_TTL
              value: "60000"
            {{- if (index .Values $appSection "fakeTime" "enabled") }}
            - name: LD_PRELOAD
              value: /usr/local/lib/libfaketime.so.1
            - name: FAKETIME
              value: {{ index .Values $appSection "fakeTime" "datetime" | quote }}
            {{- end }}
            - name: SENTRY_ENABLED
              value: {{ index .Values $appSection "sentry" "enabled" | quote }}
            - name: SENTRY_ENVIRONMENT
              value: {{ index .Values.sentry.sentryEnv | quote }}
            - name: SENTRY_DSN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: outputGatewaySentryDsn
            - name: SENTRY_DEBUG
              value: {{ index .Values.sentry.debug | quote }}
            - name: SENTRY_TRACE_RATE
              value: {{ index .Values.sentry.traceRate | quote }}
            - name: JAEGER_ENABLED
              value: {{ index .Values $appSection "opentelemetry" "enabled" | quote }}
            {{- if index .Values $appSection "opentelemetry" "enabled" }}
            - name: JAEGER_PROCESSOR
              value: {{ index .Values.opentelemetry.processor | upper | quote }}
            - name: JAEGER_AGENT_HOST
              value: {{ index .Values.opentelemetry.host | quote }}
            - name: JAEGER_AGENT_PORT
              value: {{ index .Values.opentelemetry.port | quote }}
            - name: OTEL_TRACES_SAMPLER
              value: {{ index .Values.opentelemetry.sampler | quote }}
            - name: OTEL_TRACES_SAMPLER_ARG
              value: {{ index .Values $appSection "opentelemetry" "samplerArg" | quote }}
            - name: TELEMETRY_LOG_LEVEL
              value: {{ index .Values.opentelemetry.logLevel | quote }}
            {{- end }}
            - name: METRICS_ENABLED
              value: {{ index .Values $appSection "metrics" "enabled" | quote }}
            - name: METRICS_PORT
              value: {{ index .Values $appSection "metrics" "port" | quote }}
            - name: METRICS_PREFIX
              value: {{ index .Values $appSection "metrics" "prefix" | quote }}
            - name: VEHICLE_POSITIONS_IS_AIR_COND_ENABLED
              value: {{ if (index .Values $appSection "vehiclePositions" "isAirCondEnabled") }}"true"{{ else }}"false"{{ end }}
            {{- if index . "Values" $appSection "inspector" "enabled" }}
            - name: INSPECTOR_HOST
              value: {{ index . "Values" $appSection "inspector" "host" | quote }}
            {{- end }}
            {{- if index .Values $appSection "extraEnvVars" }}
            {{- index .Values $appSection "extraEnvVars" |  toYaml | nindent 12 }}
            {{- end }}
          resources:
            {{- index .Values $appSection "resources" | toYaml | nindent 12 }}
          securityContext:
            allowPrivilegeEscalation: false
            runAsNonRoot: true
            runAsUser: 1001
            capabilities:
              drop: [ "all" ]
          {{- if index . "Values" $appSection "probes" "enabled" }}
          readinessProbe: {{ index .Values $appSection "readinessProbe" | toYaml | nindent 12 }}
          livenessProbe: {{ index .Values $appSection "livenessProbe" | toYaml | nindent 12 }}
          {{- end }}
          {{- if index . "Values" $appSection "extraConfig" }}
          volumeMounts:
            {{- include "golemio.extraConfig.volumeMounts" (dict "root" .Values "section" $appSection) | nindent 12 }}
          {{- end }}
      {{- if index . "Values" $appSection "extraConfig" }}
      volumes:
      {{- include "golemio.extraConfig.volumes" (dict "root" .Values "section" $appSection) | nindent 6 }}
      {{- end }}
      tolerations:
        {{- index . "Values" $appSection "tolerations" | toYaml | nindent 8 }}
      nodeSelector:
        {{- index . "Values" $appSection "nodeSelector" | toYaml | nindent 8 }}
      {{- if (index .Values $appSection "lightship" "shutdownTimeout") }}
      terminationGracePeriodSeconds: {{ div (add (index . "Values" $appSection "lightship" "shutdownTimeout") 5000) 1000 }}
      {{- else }}
      terminationGracePeriodSeconds: 65
      {{- end }}
{{ end }}
