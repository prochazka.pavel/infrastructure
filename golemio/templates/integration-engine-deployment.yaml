{{ $appSection := "integration-engine" -}}
{{ $appName := (index . "Values" $appSection "name") -}}
{{- if index . "Values" $appSection "enabled" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $appName }}
  labels:
    app.kubernetes.io/name: {{ $appName }}
    backstage.io/kubernetes-id: {{ $appName }}
    {{- include "golemio.labels" . | nindent 4 }}
  annotations:
    rollme: {{ .Values.configurationFiles.integrationEngine | toYaml | sha256sum | trunc 8 | quote }}
    {{- include "golemio.annotations" . | nindent 4 }}
spec:
  {{- if ge (index .Values $appSection "minReplicas") (index .Values $appSection "maxReplicas") }}
  replicas: {{ index . "Values" $appSection "replicas" }}
  {{- end }}
  strategy: {{- include "golemio.rollingUpdate" (index . "Values" $appSection) | nindent 4 }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ $appName }}
      {{- include "golemio.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ $appName }}
        backstage.io/kubernetes-id: {{ $appName }}
        {{- include "golemio.labels" . | nindent 8 }}
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/" $appSection "-configmap.yaml") . | sha256sum }}
        {{- include "golemio.annotations" . | nindent 8 }}
    spec:
      automountServiceAccountToken: false
      {{- include "golemio.imagePullSecrets" (list . .Values.global.imagePullSecrets ) | indent 6 }}
      containers:
        - name: {{ $appName }}
          image: {{ include "golemio.imageName" (list . $appSection) | quote }}
          imagePullPolicy: {{ .Values.global.pullPolicy }}
          ports:
            - containerPort: {{ index . "Values" $appSection "port" }}
          env:
            - name: APP_NAME
              value: {{ index .Values $appSection "name" }}
            - name: HOPPYGO_BASE_URL
              value: https://www.hoppygo.com/
            {{- if (index .Values $appSection "lightship" "handlerTimeout") }}
            - name: LIGHTSHIP_HANDLER_TIMEOUT
              value: {{ index . "Values" $appSection "lightship" "handlerTimeout" | quote }}
            {{- end }}
            {{- if (index .Values $appSection "lightship" "shutdownDelay") }}
            - name: LIGHTSHIP_SHUTDOWN_DELAY
              value: {{ index . "Values" $appSection "lightship" "shutdownDelay" | quote }}
            {{- end }}
            {{- if (index .Values $appSection "lightship" "shutdownTimeout") }}
            - name: LIGHTSHIP_SHUTDOWN_TIMEOUT
              value: {{ index . "Values" $appSection "lightship" "shutdownTimeout" | quote }}
            {{- end }}
            - name: ROARR_LOG
              value: "false"
            - name: LOG_LEVEL
              value: {{ index .Values $appSection "logLevel" }}
            - name: DEBUG
              value: {{ index .Values $appSection "debugMode" }}
            - name: MOJEPRAHA_ENDPOINT_BASEURL
              value: https://www.mojepraha.eu
            - name: NODE_ENV
              value: {{ index .Values $appSection "nodeEnv" }}
            - name: NODE_OPTIONS
              value: {{ include "golemio.maxOldSpaceSize" (index . "Values" $appSection "resources" "limits" "memory" ) | quote}}
            - name: OPEN_STREET_MAP_API_URL_REVERSE
              value: https://nominatim.openstreetmap.org/reverse?format=json&accept-language=cs&zoom=18
            - name: OPEN_STREET_MAP_API_URL_SEARCH
              value: https://nominatim.openstreetmap.org/search?format=json&accept-language=cs
            - name: PARKINGS_PAYMENT_URL
              value: https://ke-utc.appspot.com/static/select_offstreet.html
            - name: PARKING_ZONES_PAYMENT_URL
              value: https://ke-utc.appspot.com/static/onstreet.html
            - name: PHOTON_MAP_API_URL_REVERSE
              value: {{ index .Values $appSection "photonURL" }}
            - name: PHOTON_MAP_API_KEY
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: photonIEApiKey
            - name: PORT
              value: {{ index .Values $appSection "port" | quote }}
            - name: POSTGRES_CONN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: integrationEnginePostgresConn
            - name: PGSSLMODE
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: integrationEnginePostgresSSLMode
            - name: POSTGRES_POOL_MIN_CONNECTIONS
              value: {{ index .Values $appSection "postgres" "minConnections" | quote }}
            - name: POSTGRES_POOL_MAX_CONNECTIONS
              value: {{ index .Values $appSection "postgres" "maxConnections" | quote }}
            - name: POSTGRES_POOL_IDLE_TIMEOUT
              value: {{ index .Values $appSection "postgres" "idleTimeout" | quote }}
            - name: RABBIT_CONN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: integrationEngineRabbitConn
            - name: RABBIT_EXCHANGE_NAME
              value: {{ index .Values $appSection "rabbit" "exchangeName" }}
            - name: RABBIT_CHANNEL_MAX_PREFETCH_COUNT
              value: {{ index .Values $appSection "rabbit" "maxPrefetchCount" | quote }}
            - name: REDIS_CONN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: ie-redis-conn
            {{- if (index .Values $appSection "fakeTime" "enabled" ) }}
            - name: LD_PRELOAD
              value: /usr/local/lib/libfaketime.so.1
            - name: FAKETIME
              value: {{ index .Values $appSection "fakeTime" "datetime" | quote }}
            {{- end }}
            - name: STORAGE_ENABLED
              value: {{ index .Values $appSection "azure" "enabled" | toString | quote }}
            {{- if eq (index .Values $appSection "azure" "enabled" | toString) "true" }}
            - name: AZURE_BLOB_STORAGE_CONN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: azureBlobStorageIEConn
            - name: AZURE_BLOB_STORAGE_CONTAINER_NAME
              value: {{ index .Values $appSection "azure" "container" | quote }}
            {{- end }}
            - name: TABLE_STORAGE_ENABLED
              value: {{ index .Values $appSection "azureTableStorage" "enabled" | toString | quote }}
            {{- if eq (index .Values $appSection "azureTableStorage" "enabled" | toString) "true" }}
            - name: AZURE_TABLE_ACCOUNT
              value: {{ index .Values $appSection "azureTableStorage" "account" | quote }}
            - name: AZURE_TABLE_CLIENT_ID
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: azureTableStorageIEClientId
            - name: AZURE_TABLE_TENANT_ID
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: azureTableStorageIETenantId
            - name: AZURE_TABLE_CLIENT_SECRET
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: azureTableStorageIEClientSecret
            - name: AZURE_TABLE_STORAGE_ENTITY_BATCH_SIZE
              value: {{ index .Values $appSection "azureTableStorage" "entityBatchSize" | quote }}
            {{- end }}
            - name: DATA_BATCH_SIZE
              value: "2000"
            - name: STREAM_WAIT_FOR_END_INTERVAL
              value: "500"
            - name: STREAM_WAIT_FOR_END_ATTEMPTS
              value: "1000"
            - name: SENTRY_ENABLED
              value: {{ index .Values $appSection "sentry" "enabled" | quote }}
            - name: SENTRY_ENVIRONMENT
              value: {{ index .Values.sentry.sentryEnv | quote }}
            - name: SENTRY_DSN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: integrationEngineSentryDsn
            - name: SENTRY_DEBUG
              value: {{ index .Values.sentry.debug | quote }}
            - name: SENTRY_TRACE_RATE
              value: {{ index .Values.sentry.traceRate | quote }}
            - name: JAEGER_ENABLED
              value: {{ index .Values $appSection "opentelemetry" "enabled" | quote }}
            {{- if index .Values $appSection "opentelemetry" "enabled" }}
            - name: JAEGER_PROCESSOR
              value: {{ index .Values.opentelemetry.processor | upper | quote }}
            - name: JAEGER_AGENT_HOST
              value: {{ index .Values.opentelemetry.host | quote }}
            - name: JAEGER_AGENT_PORT
              value: {{ index .Values.opentelemetry.port | quote }}
            - name: OTEL_TRACES_SAMPLER
              value: {{ index .Values.opentelemetry.sampler | quote }}
            - name: OTEL_TRACES_SAMPLER_ARG
              value: {{ index .Values $appSection "opentelemetry" "samplerArg" | quote }}
            - name: TELEMETRY_LOG_LEVEL
              value: {{ index .Values.opentelemetry.logLevel | quote }}
            {{- end }}
            - name: METRICS_ENABLED
              value: {{ index .Values $appSection "metrics" "enabled" | quote }}
            - name: METRICS_PORT
              value: {{ index .Values $appSection "metrics" "port" | quote }}
            - name: METRICS_PREFIX
              value: {{ index .Values $appSection "metrics" "prefix" | quote }}
            - name: RABBIT_MAX_RECONNECTIONS
              value: "0"
            - name: VEHICLE_POSITIONS_REDIS_EXPIRE_TIME
              value: {{ index .Values $appSection "vehiclePositions" "redisExpireTime" | quote }}
            - name: VEHICLE_POSITIONS_TCP_BUS_NEGATIVE_OFFSET
              value: "1"
            - name: VEHICLE_POSITIONS_DATA_MAINTENANCE_ENABLED
              value: {{ if (index .Values $appSection "vehiclePositions" "dataMaintenanceEnabled") }}"true"{{ else }}"false"{{ end }}
            - name: VEHICLE_POSITIONS_METRO_ARRIVAL_DELAY_ADDITION_IN_SECONDS
              value: {{ index .Values $appSection "vehiclePositions" "metroArrivalDelayInSeconds" | quote }}
            - name: VEHICLE_POSITIONS_METRO_DEPARTURE_DELAY_ADDITION_IN_SECONDS
              value: {{ index .Values $appSection "vehiclePositions" "metroDepartureDelayInSeconds" | quote }}
            - name: ROPID_PRESET_LOG_BATCH_SIZE
              value: {{ index .Values $appSection "vehiclePositions" "logBatchSize" | quote }}
            - name: ROPID_PRESET_SEND_LOGS_LOCK_TIMEOUT
              value: {{ index .Values $appSection "vehiclePositions" "logLockTimeout" | quote }}
            {{- if index .Values $appSection "extraEnvVars" }}
            {{- index .Values $appSection "extraEnvVars" |  toYaml | nindent 12 }}
            {{- end }}
          resources:
            {{- index . "Values" $appSection "resources" | toYaml | nindent 12 }}
          securityContext:
            allowPrivilegeEscalation: false
            runAsNonRoot: true
            runAsUser: 1001
            capabilities:
              drop: [ "all" ]
          {{- if index . "Values" $appSection "probes" "enabled" }}
          readinessProbe: {{ index .Values $appSection "readinessProbe" | toYaml | nindent 12 }}
          livenessProbe: {{ index .Values $appSection "livenessProbe" | toYaml | nindent 12 }}
          {{ end }}
          volumeMounts:
            - name: {{ $appName }}-config
              mountPath: /app/config/k6.json
              subPath: k6.json
              readOnly: true
            - name: {{ $appName }}-config
              mountPath: /app/config/queuesBlacklist.json
              subPath: queuesBlacklist.json
              readOnly: true
            - name: {{ $appName }}-config
              mountPath: /app/config/saveRawDataWhitelist.json
              subPath: saveRawDataWhitelist.json
              readOnly: true
            - name: {{ $appName }}-secret
              mountPath: /app/config/moduleConfig.json
              subPath: moduleConfig
              readOnly: true
            - name: {{ $appName }}-secret
              mountPath: /app/config/google-playstore-keyfile.json
              subPath: google-playstore
              readOnly: true
            {{- include "golemio.extraConfig.volumeMounts" (dict "root" .Values "section" $appSection) | nindent 12 }}
      volumes:
      - name: {{ $appName }}-config
        configMap:
          name: {{ $appName }}-config
          items:
            - key: k6
              path: k6.json
            - key: queue-black-list
              path: queuesBlacklist.json
            - key: raw-data-white-list
              path: saveRawDataWhitelist.json
            {{- range $key,$value := index .Values $appSection "extraConfig" }}
            {{- range $inkey,$invalue := $value }}
            {{- if  not $invalue.secret }}
            - key: {{ $inkey | quote }}
              path: {{ (split "." $inkey)._0 | quote }}
            {{- end }}
            {{- end }}
            {{- end }}
      - name: {{ $appName }}-secret
        secret:
          secretName: {{.Values.global.secretName }}
          items:
            - key: "moduleConfig.json"
              path: "moduleConfig"
            - key: "google-playstore.json"
              path: "google-playstore"
            {{- range $key,$value := index .Values $appSection "extraConfig" }}
            {{- range $inkey,$invalue := $value }}
            {{- if $invalue.secret }}
            - key: {{ $inkey | quote }}
              path: {{ (split "." $inkey)._0 | quote }}
            {{- end }}
            {{- end }}
            {{- end }}
      tolerations:
        {{- index . "Values" $appSection "tolerations" | toYaml | nindent 8 }}
      nodeSelector:
        {{- index . "Values" $appSection "nodeSelector" | toYaml | nindent 8 }}
      {{- if (index .Values $appSection "lightship" "shutdownTimeout") }}
      terminationGracePeriodSeconds: {{ div (add (index . "Values" $appSection "lightship" "shutdownTimeout") 5000) 1000 }}
      {{- else }}
      terminationGracePeriodSeconds: 65
      {{- end }}
{{ end }}
