{{ $appSection := "cron" -}}
{{ $appName := (index . "Values" $appSection "name") -}}
{{- if index . "Values" $appSection "enabled" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $appName }}
  labels:
    app.kubernetes.io/name: {{ $appName }}
    {{- include "golemio.labels" . | nindent 4 }}
  annotations:
    rollme: {{ .Values.configurationFiles.cronTasks | toYaml | sha256sum | trunc 8 | quote }}
    {{- include "golemio.annotations" . | nindent 4 }}
spec:
  replicas: 1
  strategy: {{- include "golemio.rollingUpdate" (index . "Values" $appSection) | nindent 4 }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ $appName }}
      {{- include "golemio.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ $appName }}
        {{- include "golemio.labels" . | nindent 8 }}
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/" $appName "-configmap.yaml") . | sha256sum }}
        {{- include "golemio.annotations" . | nindent 8 }}
    spec:
      automountServiceAccountToken: false
      {{- include "golemio.imagePullSecrets" (list . .Values.global.imagePullSecrets ) | nindent 6 }}
      containers:
        - name: {{ $appName }}
          image: {{ include "golemio.imageName" (list . $appSection) | quote }}
          imagePullPolicy: {{ .Values.global.pullPolicy }}
          env:
            - name: APP_NAME
              value: {{ index .Values $appSection "name" }}
            - name: LOG_LEVEL
              value: {{ .Values.cron.logLevel }}
            - name: NODE_ENV
              value: {{ .Values.cron.nodeEnv }}
            - name: RABBIT_CONN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: cronRabbitConn
            - name: JAEGER_ENABLED
              value: {{ index .Values $appSection "opentelemetry" "enabled" | quote }}
            {{- if index .Values $appSection "opentelemetry" "enabled" }}
            - name: JAEGER_PROCESSOR
              value: {{ index .Values.opentelemetry.processor | upper | quote }}
            - name: JAEGER_AGENT_HOST
              value: {{ index .Values.opentelemetry.host | quote }}
            - name: JAEGER_AGENT_PORT
              value: {{ index .Values.opentelemetry.port | quote }}
            - name: OTEL_TRACES_SAMPLER
              value: {{ index .Values.opentelemetry.sampler | quote }}
            - name: OTEL_TRACES_SAMPLER_ARG
              value: {{ index .Values $appSection "opentelemetry" "samplerArg" | quote }}
            - name: TELEMETRY_LOG_LEVEL
              value: {{ index .Values.opentelemetry.logLevel | quote }}
            {{- end }}
          resources:
            {{ index .Values $appSection "resources" | toYaml | nindent 12 }}
          securityContext:
            allowPrivilegeEscalation: false
            runAsNonRoot: true
            runAsUser: 1001
            capabilities:
              drop: [ "all" ]
          volumeMounts:
            - name: {{ $appName }}-config
              mountPath: /app/config/cronTasks.json
              subPath: cron-tasks.json
              readOnly: true
      volumes:
      - name: {{ $appName }}-config
        configMap:
          name: {{ $appName }}-config
          items:
            - key: cron-tasks
              path: cron-tasks.json
{{ end }}
