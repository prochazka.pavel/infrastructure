{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "golemio.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "golemio.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "golemio.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "golemio.selectorLabels" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ include "golemio.name" . }}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "golemio.labels" -}}
{{ include "golemio.selectorLabels" . }}
helm.sh/chart: {{ include "golemio.chart" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- end -}}

{{/*
Common annotations
*/}}
{{- define "golemio.annotations" -}}
{{- end -}}

{{/*
Custom helpers
*/}}

{{- define "golemio.imagePullSecrets" -}}
  {{- $top := index . 0 -}}
  {{- $pullSecrets := index . 1 -}}

  {{- if (not (empty $pullSecrets)) }}
imagePullSecrets:
    {{- range $pullSecrets | uniq }}
  - name: {{ . }}
    {{- end }}
  {{- end }}
{{- end -}}

{{- define "golemio.imageName" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{ index $top "Values" "global" "imageRegistry" }}/{{ index $top "Values" $appSection "repository" }}{{ if index $top "Values" $appSection "branch" }}/{{ index $top "Values" $appSection "branch" }}{{- end -}}:{{ index $top "Values" $appSection "tag" }}
{{- end -}}

{{- define "golemio.readinessProbe" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
httpGet:
  path: /ready
  port: 9000
initialDelaySeconds: {{ index $top "Values" "global" "readinessProbe" "initialDelaySeconds" }}
periodSeconds: {{ index $top "Values" "global" "readinessProbe" "periodSeconds" }}
{{- end -}}

{{- define "golemio.livenessProbe" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
httpGet:
  path: /live
  port: 9000
{{- end -}}

{{- define "golemio.rollingUpdate" -}}
rollingUpdate:
  maxUnavailable: {{ if gt (index . "replicas" | toDecimal) 1 -}}1{{- else -}}0{{- end }}
  maxSurge: 1
{{- end -}}

{{/*
Renders a value that contains template.
Usage:
{{ include "common.tplvalues.render" ( dict "value" .Values.path.to.the.Value "context" $) }}
*/}}
{{- define "common.tplvalues.render" -}}
    {{- if typeIs "string" .value }}
        {{- tpl .value .context }}
    {{- else }}
        {{- tpl (.value | toYaml) .context }}
    {{- end }}
{{- end -}}

{{- define "golemio.maxOldSpaceSize" -}}
{{- $memoryLimit := regexFind "[0-9]+" . }}
{{- $reserveCoeficient := 0.25 }}
{{- $memoryReserve := max 64 (min 512 (mulf $reserveCoeficient $memoryLimit)) }}
{{- printf "--max-old-space-size=%d" (subf $memoryLimit $memoryReserve | int) -}}
{{- end -}}

{{- define "golemio.extraConfig.volumeMounts" -}}
{{- $app := .section -}}
{{- $appName := (index .root $app "name") -}}
{{- range $key,$value := index .root $app "extraConfig" -}}
{{- range $inkey,$invalue := $value -}}
{{- if $invalue.secret }}
- name: {{ $appName }}-secret
{{- else }}
- name: {{ $appName }}-config
{{- end }}
  mountPath: {{ $invalue.path }}{{ $inkey }}
  subPath: {{ (split "." $inkey)._0 }}
  readOnly: true
{{- end }}
{{- end }}
{{- end }}

{{- define "golemio.extraConfig.volumes" -}}
{{- $app := .section -}}
{{- $appName := (index .root $app "name") -}}
{{- range $key,$value := index .root $app "extraConfig" -}}
{{- range $inkey,$invalue := $value -}}
{{- if $invalue.secret -}}
- name: {{ $appName }}-secret
  secret:
    secretName: {{ $.root.global.secretName }}
{{- else }}
- name: {{ $appName }}-config
  configMap:
    name: {{ $appName }}-config
{{- end }}
    items:
      - key: {{ $inkey | quote }}
        path: {{ (split "." $inkey)._0 | quote }}
{{- end }}
{{- end }}
{{- end }}
