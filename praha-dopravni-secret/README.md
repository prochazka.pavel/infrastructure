# Rabin
```bash
helm upgrade --install --namespace frontend --values ../rabin/praha-dopravni/values-secret.yaml praha-dopravni-secret ./praha-dopravni-secret
```

# Golem
```bash
helm upgrade --install --namespace frontend --values ../golem/praha-dopravni/values-secret.yaml praha-dopravni-secret ./praha-dopravni-secret
```
