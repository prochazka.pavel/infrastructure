---
- name: Generate all environment files
  hosts: monitor
  gather_facts: no
  tasks:
# GENERAL
    - name: Create a directory if it does not exist
      file:
        path: "{{ base_path }}/{{ item }}"
        state: directory
        recurse: true
        mode: '0755'
      loop: ["deploy", "stacks", "configs", "secrets"]
      tags:
        - base

    - name: Generate shell script files
      template:
        src: "templates/dataplatform/{{ item }}.j2"
        dest: "{{ base_path }}/{{ item }}.sh"
        mode: '755'
      loop: ["create-networks", "create-all", "remove-all"]
      tags:
        - base

# CONFIGS
    - name: Generate common config files
      template:
        src: "{{ item }}"
        dest: "{{ base_path }}/configs/{{ item | basename }}"
      loop: "{{ lookup('fileglob', 'templates/dataplatform/configs/*', wantlist=True) }}"
      tags:
        - configAndSecret

    - name: Generate project specific configs
      template:
        src: "{{ item }}"
        dest: "{{ base_path }}/configs/"
      loop: "{{ lookup('fileglob', 'files/{{ project_name }}/configs/*', wantlist=True) }}"
      tags:
        - configAndSecret

# SECRETS
    - name: Generate common secrets files
      template:
        src: "{{ item }}"
        dest: "{{ base_path }}/secrets/{{ item | basename }}"
      loop: "{{ lookup('fileglob', 'templates/dataplatform/secrets/*', wantlist=True) }}"
      tags:
        - configAndSecret

    - name: Generate project specific secrets
      template:
        src: "{{ item }}"
        dest: "{{ base_path }}/secrets/"
      loop: "{{ lookup('fileglob', 'files/{{ project_name }}/secrets/*', wantlist=True) }}"
      tags:
        - configAndSecret

# CONFIGS & SECRETS MTIMES
    - name: List of configs & secrets
      find:
        paths:
          - "{{ base_path }}/configs"
          - "{{ base_path }}/secrets"
        file_type: file
      register: files_configs_and_secrets
      tags:
        - configAndSecret

    - name: "Register last modification of config/secret files filename=>mtime"
      set_fact:
        mtimes: >
          {{ mtimes|default({}) | combine({(item.path|dirname|basename)+'/'+(item.path|basename): ('%Y-%m-%d-%H-%M-%S' | strftime(item.mtime|int|string)) }) }}
      loop: "{{ files_configs_and_secrets.files|flatten(levels=1) }}"
      tags:
        - configAndSecret

    - name: Show mtimes
      debug:
        var: mtimes

# STACKS
    - name: Generate common stack files
      template:
        src: "{{ item }}"
        dest: "{{ base_path }}/stacks/{{ item | basename }}"
      loop: "{{ lookup('fileglob', 'templates/dataplatform/stacks/*', wantlist=True) }}"
      tags:
        - configAndSecret

    - name: Copy stacks
      template:
        src: "{{ item }}"
        dest: "{{ base_path }}/stacks/"
      loop: "{{ lookup('fileglob', 'files/{{ project_name }}/stacks/*', wantlist=True) }}"
      tags:
        - configAndSecret

    - name: Final list of remote stack files
      find:
        recurse: yes
        paths: "{{ base_path }}/stacks"
        file_type: file
      register: remote_stack_files
      tags:
        - configAndSecret

    - name: Generate deploy files
      template:
        src: "templates/dataplatform/stack_deploy.j2"
        dest: "{{ base_path }}/deploy/{{ (item.path | basename | splitext)[0] }}.sh"
        mode: '750'
      loop: "{{ remote_stack_files.files|flatten(levels=1) }}"
      tags:
        - base
