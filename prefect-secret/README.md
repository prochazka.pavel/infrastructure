# Prefect secret

## Rabin
```bash
helm upgrade --install --namespace prefect --values ../rabin/prefect/values-secret.yaml prefect-secret prefect-secret
```


## Golem
```bash
helm upgrade --install --namespace prefect --values ../golem/prefect/values-secret.yaml prefect-secret prefect-secret
```
