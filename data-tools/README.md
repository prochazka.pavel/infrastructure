# Data tools


## Rabin
```bash
helm upgrade --install --values data-tools/values-development-versions.yaml --values cluster_development/data-tools_data-tools-values.yaml --namespace data-tools data-tools data-tools
```

## Golem
```bash
helm upgrade --install --values data-tools/values-master-versions.yaml --values cluster_production/data-tools_data-tools-values.yaml --namespace data-tools data-tools data-tools
```
