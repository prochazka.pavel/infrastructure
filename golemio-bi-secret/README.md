# Rabin
```bash
helm upgrade --install --namespace frontend --values ../rabin/golemio-bi/values-secret.yaml golemio-bi-secret golemio-bi-secret
```

# Golem
```bash
helm upgrade --install --namespace frontend --values ../golem/golemio-bi/values-secret.yaml golemio-bi-secret golemio-bi-secret
helm upgrade --install --namespace frontend --values ../golem/golemio-bi2/values-secret.yaml golemio-bi2-secret golemio-bi-secret
```
