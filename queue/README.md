# RabbitMQ

## Operator VS single instance
https://github.com/bitnami/charts/tree/main/bitnami/rabbitmq-cluster-operator#differences-between-the-bitnami-rabbitmq-chart-and-the-bitnami-rabbitmq-operator-chart

https://github.com/bitnami/charts/tree/main/bitnami/rabbitmq

https://hub.docker.com/r/bitnami/rabbitmq/tags?page=1&name=3

## Install
1. Add helm repo

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

2. Install helm
```bash
# create storage class
kaf queue/storageClass.yaml

# dev
helm upgrade --install -n queue rabbitmq bitnami/rabbitmq --values queue/rabbitmq-values-dev.yaml \
  --version 14.1.1

# prod
helm upgrade --install -n queue rabbitmq bitnami/rabbitmq --values queue/rabbitmq-values-prod.yaml \
  --version 12.9.2


# parking-dev
helm upgrade --install -n parking-dev rabbitmq bitnami/rabbitmq --values queue/rabbitmq-values-parking-dev.yaml \
  --version 14.1.1


# parking-prod
helm upgrade --install -n parking rabbitmq bitnami/rabbitmq --values queue/rabbitmq-values-parking-prod.yaml \
  --version 12.9.2
```
RabbitMQ can be accessed within the cluster on port at `rabbitmq.queue.svc.cluster.local`.

## Login

Credentials: See ../security/golemio-rabin/queue/values-secret.yaml

Most applications use 'rabbit' user which is the administrator of RabbitMQ but vehicle-positions use 'vehicle-positions' user (also administrator due to lack of info about required permissions).
For password used by the application see `kubectl get secret golemio-secret -o go-template='{{.data.vehiclePositionsRabbitConn | base64decode}}'`.

## Setup port forwarding for Management UI

```bash
kubectl port-forward --namespace queue svc/rabbitmq 15672
```

## development cluster

Změna typu clusteru na RAM. Lze pouze u prvních 2 ze 3.

```
rabbitmqctl stop_app
rabbitmqctl change_cluster_node_type ram
rabbitmqctl start_app
```
```bash
kubectl exec -ti rabbitmq-1 -- bash
```

a pak uvnitr
```bash
rabbitmq-queues grow "rabbit@rabbitmq-1.rabbitmq-headless.queue.svc.cluster.local" "all" --vhost-pattern "/" --queue-pattern ".*"
```