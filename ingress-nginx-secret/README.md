# Ingress nginx secrets
Helm slouzi k instalaci `ClusterIssuer` na ověřování Lets encrypt certifikátů přes HTTP nebo DNS challenge. 

Heslo pro doplnění do values se získává z KeyVaultu začínajícím `dnsChallenge-` a má platnost 3 roky.

https://cert-manager.io/docs/configuration/acme/dns01/azuredns/#service-principal

```bash
APP_NAME=dnsChallenge-XXXX

AZ_DATA=$(az ad sp list --display-name $APP_NAME | jq ".[0]" ) 
CLIENT_ID=$(echo $AZ_DATA | jq -r '.appId')
SUBSCRIPTION_ID=$(az account show --output json | jq -r '.id')

echo "subscriptionID: $SUBSCRIPTION_ID"
echo "clientID: $CLIENT_ID"   
echo "secret: look into key vault $APP_NAME"
```

## Default certificate domain
Zakládá K8S certifikát, který se použije jako defaultní v ingress-nginx. Je to nezbytné při využití Azure Frontdoor CND s nastavením komunikace FD->nginx jako HTTPS.

# Rabin
```bash
helm upgrade --install --namespace ingress --values ../rabin/ingress-nginx/values-secret.yaml ingress-nginx-secret ./ingress-nginx-secret
```

# Golem
```bash
helm upgrade --install  --namespace ingress --values ../golem/ingress-nginx/values-secret.yaml ingress-nginx-secret ./ingress-nginx-secret
```
