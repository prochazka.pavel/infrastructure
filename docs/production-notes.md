# Production notes

## RabbitMQ
- use RabbitMQ as at least 2-node cluster - migration from single mode to cluster mode is unpleasant
- setup your RabbitMQ queues and exchange servers as HA https://www.rabbitmq.com/ha.html , https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/135