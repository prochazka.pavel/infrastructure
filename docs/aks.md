# Azure Kubernetes Services

## Upgrade OS na K8s uzlech


https://learn.microsoft.com/en-us/azure/aks/node-image-upgrade#check-for-available-node-image-upgrades

```bash
az aks nodepool get-upgrades \
--subscription 4f46bf2a-5ee0-4862-b162-3fc80c2c57c9 \
--resource-group rg-aks-dp-rabin \
--cluster-name dp-rabin \
--nodepool-name c8m32system

az aks nodepool show \
--subscription 4f46bf2a-5ee0-4862-b162-3fc80c2c57c9 \
--resource-group rg-aks-dp-rabin \
--cluster-name dp-rabin \
--name c8m32system \
--query nodeImageVersion

az aks upgrade \
--subscription 4f46bf2a-5ee0-4862-b162-3fc80c2c57c9 \
--resource-group rg-aks-dp-rabin \
--name dp-rabin \
--node-image-only
```