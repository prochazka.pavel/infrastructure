# API Gateway

- [Doporučení Alibaba](https://www.alibabacloud.com/knowledge/hot/best-open-source-api-gateways-to-consider)

## Co hledáme?
- neplatí se dle počtu requestů, protože těch máme mnoho
- jak vyřešit elegatně přihlašování (nyní JWT s ověřením u třetí strany) - "Token Introspection"
- ideálně rozšiřitelné (pluginy) přes JS/TS
- zatím jsme neřešili OAuth jako součást pohledávky

<!---
# Název
- URL:
- Opensource:

### Poznámky
- @todo 

### Výhody

### Nevýhody
-->

## Ory
# Název
- URL: https://www.ory.sh/open-source/
- Opensource: Ano, nenašel jsem co by open source nepodporoval oproti placené verzi.

### Poznámky
- @todo 

### Výhody
- Není to Java
- Je to mikroservisní architektura
- Podobné tomu na co jsme zvyklí
- Připravené pro cloud (helm charts)

### Nevýhody
- Je potřeba si nastavit vlastní GUI dle připravených šablon, aby to zapadalo do našeho workflow. 

## Kong
- URL: https://konghq.com/
- Opensource: Ano, ale některé [pluginy jsou placené](https://docs.konghq.com/hub/?tier=paid%2Cpremium%2Centerprise).

### Poznámky
- @todo [GUI](https://docs.konghq.com/gateway/3.5.x/kong-manager-oss/) má verzi OSS, ale je potřeba vyzkoušet její omezení
- [srovnání verzí pro self-managed](https://docs.konghq.com/gateway/3.5.x/#self-managed)
- @toto ověřit jak ověřovat uživatele po zaslání JWT ještě proti DB

### Výhody
- výkonná API GW
- enterprise ověřené řešení

### Nevýhody
- není veřejný [ceník](https://konghq.com/pricing)
- vlastní pluginy jsou v [Lua](https://docs.konghq.com/gateway/latest/plugin-development/custom-logic/)


## Tyk
- URL: https://tyk.io/
- Opensource: Ano, [GUI není opensource](https://tyk.io/docs/apim/open-source/#tyk-open-source)

### Poznámky
- [Srovnání s konkurencí](https://tyk.io/comparison/)
- @toto ověřit jak ověřovat uživatele po zaslání JWT ještě proti DB
 
### Výhody
- výkonná API GW (o něco méně než Kong)
- enterprise ověřené řešení
- pluginy nejsou zpoplatněny zvlášt jako u Kong
- cena je fixní, nezávisí na počtu requestů
### Nevýhody
- [GUI není open source](https://tyk.io/docs/tyk-stack/)
- neveřejný [ceník pro self-managed](https://tyk.io/pricing-self-managed/)


## Azure API Management
- URL: https://learn.microsoft.com/en-us/azure/api-management/api-management-key-concepts
- Opensource: Ne
### Poznámky
- @todo
### Výhody
- výkonná API GW
- enterprise ověřené řešení
- více možností jak počítat cenu
- je součástí Azure

### Nevýhody
- nelze připojit 3rd party na doupřesnění opravnění => nutné změnit logiku ověřování
- není přenositelné mimo Azure 


## KrakenD
- URL: https://www.krakend.io/
- Opensource: Ano, některé pluginy jsou jen v Enterprise
- Demo: https://designer.krakend.io/#!/service

### Poznámky
- @toto ověřit jak ověřovat uživatele po zaslání [JWT ještě proti DB](https://www.krakend.io/docs/authorization/jwt-validation/) - přístup na DB by mohl řešit [Key Cloak](https://www.krakend.io/docs/authorization/keycloak/)
- [srovnání Community a Enterprise](https://www.krakend.io/features/)
-  dle emailu (KrakenD Enterprise information for Operator ICT | ze dne 14.2.2024) začínají na 12,K Euro/rok pro Starter
- @todo určitě ještě více prozkoumat

### Výhody
- GUI je open-source
- [vysoce výkonná API GW](https://www.krakend.io/docs/benchmarks/api-gateway-benchmark/#requests-per-second)
- Stateless (nemá databázi), tedy dobře škálovatelná a nezávislá

### Nevýhody
- některé pluginy jsou v Enterprise 
  - [API–keys](https://www.krakend.io/docs/enterprise/authentication/api-keys/) - hodně podobné naší aktuální verzi přihlšování
  - [Rate limit](https://www.krakend.io/docs/enterprise/throttling/global-rate-limit/)
  - [GZip](https://www.krakend.io/docs/enterprise/service-settings/gzip/) - lze řešit asi na ingressu
- vlastní pluginy pouze v [Lua a Go](https://www.krakend.io/docs/extending/)


# Gravitee
- URL: https://gravitee.io
- Opensource: [Ano](https://documentation.gravitee.io/platform-overview/gravitee-essentials/gravitee-offerings-ce-vs-ee)
- Demo: [lokální Docker](https://docs.gravitee.io/apim/3.x/apim_installation_guide_docker_compose_quickstart.html)

### Poznámky
- @todo zjistit co všechno nám chybí z Enterprise
- @todo určitě ještě více prozkoumat sluzby [AM](https://documentation.gravitee.io/am/getting-started/install-and-upgrade-guides/run-in-docker/docker-compose-install) (admin /adminadmin) a [APIM](https://documentation.gravitee.io/apim/getting-started/install-guides/install-on-docker/custom-install-with-docker-compose)
### Výhody
-

### Nevýhody
- [vlastní role pouze v Enterprise](https://documentation.gravitee.io/apim/guides/administration/user-management-and-permissions#roles)
- složitější struktura komponent
- pracuje to s Elasticsearch, který nemáme, ale není nezbytný
- pracuje s Mongo a mělo by umět i PSQL, ale v demo Dockeru to není
- běží na Java


# Gloo Edge
- URL: https://docs.solo.io/gloo-edge/latest/
- Opensource: Ano

### Poznámky
- @todo zatím jsem nestihl prozkoumat, ale stojí to za to
- Gloo Gateway (základní, ale taky prozkoumat) je podmnožinou Gloo Edge

### Výhody
- Kubernetes native

### Nevýhody
- GUI je pouze v enterprise
- neveřejný ceník


## Fusio
- URL: https://www.fusio-project.org
- Opensource: Ano
- Demo: https://www.fusio-project.org/demo

### Poznámky

### Výhody
- psané v JS/TS
- plně open source
- rozšiřitelná logika zpracování requestů přes různé actions

### Nevýhody
- nízká [aktivita vývoje](https://github.com/apioo/fusio/graphs/commit-activity) a počet [contributorů](https://github.com/apioo/fusio/graphs/contributors), ale má 1.7K github stars
- nejasná výkonnost


# WSO2 API Microgateway
- URL: https://mg.docs.wso2.com/en/latest/
- Opensource: Ano

### Poznámky
- @todo nezkoumal jsem vůbec

### Výhody


### Nevýhody



# API Umbrella
- URL: https://github.com/NREL/api-umbrella/tree/main
- Opensource: Ano

### Poznámky
- @todo moc jsem nezkoumal, ale vypadá to neudržovaně

### Výhody
- používá https://api.data.gov/

### Nevýhody



# Ostatní
- [Apiman](https://github.com/apiman/apiman) - minimální vývoj, běží na Javě
- [Vulcand](https://github.com/vulcand/vulcand/) - žádný vývoj několik let
- [Goku API Gateway](https://github.com/eolinker/goku_lite) - žádný vývoj několik let
- [APache APISIX](https://apisix.apache.org/) - zkoumal Tom a nevyhovovalo to, [analýza v nodejs chapter](https://gitlab.com/operator-ict/oddeleni-vyvoje/extras/nodejs-chapter/-/blob/main/tasks/permission-proxy/apisix_analyza_2023-07-28.md?ref_type=heads)
