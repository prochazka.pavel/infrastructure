# DNS
Pro přístup ke službám používáme nginx-ingress ve 3 instancích.
- Public - dostupný z internetu
- Internal - dostupný z vnitřní sítě
- Local - dostupný pouze v rámci clusteru

## Public DNS
Nastaveno na Cloudflate nebo Azure DNS. 

## Private DNS
Využívá interní DNS a přes Azure DNS resolver i Azure Private DNS pro některé domény.

## K8s local dns
Popsáno v ../core-dns/