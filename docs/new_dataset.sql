--------------------------------------------------------------------------------------
--	Vzor vytvoření nové datové sady
--------------------------------------------------------------------------------------

--	Založení záznamu v meta.dataset

INSERT INTO meta.dataset(
	--	id_dataset, --nevyplňovat - autoincrement
	code_dataset, 
	name_dataset, 
	descrition, 
    retention_days, 
	h_retention_days,
	created_at
	)
	VALUES (
	--id_dataset, 
	'nazevsady'		--code_dataset, 
	'Název sady'	--name_dataset, 
	'Popis sady',	--descr, 
	null,			--ret_days, 
	null,			--h_ret_days, 
	now() 			--created_at
	);
	
/*	výběr id:
select id_dataset from meta.dataset where code_dataset = 'nazevsady';
*/	

-- script pro vytoření tabulek
-- Table: public.test

-- DROP TABLE public.test;

CREATE TABLE public.test
(
    nazev character varying COLLATE pg_catalog."default" NOT NULL,
    data json,
    CONSTRAINT test_pkey PRIMARY KEY (nazev)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.test
    OWNER to postgres;
COMMENT ON TABLE public.test
    IS 'testovaci';
	
-- volitelně: 	přidání auditních sloupců
SELECT meta.add_audit_field('p_tabel_schema','p_table_name');

-- volitelně:	vytvoření historické tabulky
SELECT meta.hist('public','test');

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract, 
	id_dataset, 
	id_db, 
	description, 
	schema_extract, 
	retention_days, 
	h_retention_days, 
	created_at)
	
	VALUES (
	'name_extract',		-- name_extract, 
select id_dataset from meta.dataset where code_dataset = 'nazevsady',		--;id_dataset, 
	1,		--	1 - postgres, 2 mongodb_db	id_db, 
	'popis',	--descr, 
	'schema',	--schema_extract, 
	null,	--ret_days, 
	null,	--h_ret_days, 
	now());

--------------------------------------------------------------------------------------
