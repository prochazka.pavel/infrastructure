# Getting started

## Vocabulary
TBD

## Prerequisites

## Setup relational databases
Use Adminer to login into PostgreSQL. Credentials are set in the file `database.yml` by constants `POSTGRES_USER` and `POSTGRES_PASSWORD`.

![Adminer login screen](img/adminer-postgres-login.png)

When you are logged in, create new databases for dataplatform (recommended: `dataplatform`) and for permission proxy (recommended: `permission_proxy`)

```
CREATE DATABASE dataplatform;
CREATE DATABASE permission_proxy;

CREATE EXTENSION postgis; -- activate extension postgis
```

Copy&paste SQL script from `init-database/init.sql` in the Adminer and change default values (usernames, passwoeds, ...). It creates users for modules with correct privileges. These credentials setup in file `gateways.yml`. 


## Database migrations
TBD It runs database migrations which prepare whole PostgreSQL database structure to you.

Result will be seen in Adminer (`http://IP_ADDRESS_OF_CLUSTER:8080`).



# How run project on bare metal

## Prerequisites
1. A virtual machine with ssh access and root privileges.

## 3rd party software
For some the next software you have to create users with proper access rights and then setup it in each module. How to setup credentials is described in each module.

- [Node.js](https://nodejs.org/en/download/) 18+
- [RabbitMQ](https://www.rabbitmq.com/download.html)
- [Redis](https://redis.io/topics/quickstart) 6+
- [PostgreSQL](https://www.postgresql.org/docs/10/tutorial-install.html) 13.9+ and [PostGIS extension](https://postgis.net/install/)  2.5+
- [Grafana](https://grafana.com/docs/installation/) - 9+, optional
- [Adminer](https://www.adminer.org/cs/) - optional, require PHP server

## Setup relational databases

- Same as `Setup relational databases` in Docker installation.
- Run [database migrations](https://gitlab.oict.cz/data-platform/code/schema-definitions/blob/development/docs/migrations.md#pomoc%C3%AD-npm)
- [Permission proxy](https://gitlab.oict.cz/data-platform/code/permission-proxy#installation).

## Module installation
Installation and setup of each module is describe in a repository of that module.
