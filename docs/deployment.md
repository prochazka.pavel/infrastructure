# Nasazení nové verze u již nastavených repositářů
1. Commit do development či master větve.
1. Propojení automaticky aktualizuje soubor <název_projektu>/values-<branch>-versions.yaml (např.: covid/values-development-versions.yaml) kam se zapíše aktuálně poslední tag zbuildované image. S tím se releasuje nová verze Helm šablony.
1. V rámci CI/CD je připraven job pro deployment. Ten je potřeba **ručně spustit po proběhnutí předchozích kroků** (viz obrázek).

![Ukázka CI/CD pro ruční spuštění deploymentu](img/cicd-deploy-example.png)


# Připojení nového repositáře
1. Repositář s aplikací musí mít nastavené propojení na tento repositář ([ukázka](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/blob/4f5d11b04d28aa4575e5409704abed16d3a3cd6a/.gitlab-ci.yml#L20)).
1. V tomto repositáři založte soubory ve formátu <název_projektu>/values-<branch>-versions.yaml a musí obsahovat již nějaký yaml obsah (bez toho spadne autoupdate poslední verze).