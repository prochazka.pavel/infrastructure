# Release of new major version 


## 1. Create docker image v2
TBD

Now we have separated docker images for each version.

## 2. Create new K8s deployment
TBD

### Redis
Set a different number of Redis database than previous versions! To set a specific database number use the env `REDIS_CONN` with db number in the URL, e.g. to use db number `2` set url to `redis://localhost/2`.

## 3. Routing
In permission-proxy, get all current routes for current version and create the new ones (via API). New routes will contains `target_url: http://output-gateway-v2:3000`.

## 4. Database migrations
For the backward compatibility of current API you should do just nondestructive changes. When you stop obsolete API, is strictly recommended add migration for removal out dated structures and data.
