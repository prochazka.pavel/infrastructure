# FTP

## Instalation
Create new postgres database and run:
```sql
CREATE EXTENSION pgcrypto;

CREATE TABLE "public"."users" (
    "username" text NOT NULL,
    "password" text NOT NULL,
    "home_dir" text NOT NULL,
    "quota_size" integer DEFAULT '500' NOT NULL,
    CONSTRAINT "users_pkey" PRIMARY KEY ("username")
) WITH (oids = false);

COMMENT ON COLUMN "public"."users"."quota_size" IS 'MB';

```

## Add new user
```sql
INSERT INTO "users" ("username", "password", "home_dir", "quota_size") 
VALUES ('a2', crypt('pass', gen_salt('bf')), '', '500');
```


## How rebuild FTP service
1. create local folder `ftp_path` for example `/data/ftp`
1. deploy docker stack (https://github.com/crazy-max/docker-pure-ftpd)
1. connect to your host with pureftpd container and join into it by `docker exec -ti $(docker ps | grep pureftpd | awk '{print $1}') sh`
    - create TLS certificate and fill information of your organization
    ```bash
    openssl dhparam -out /data/pureftpd-dhparams.pem 2048
    openssl req -x509 -nodes -newkey rsa:2048 -sha256 -days 1095 -keyout /data/pureftpd.pem -out /data/pureftpd.pem```
1. open port 16421 for ftp connection and 30000-30099 ports for passive connection
1. Tadáá! We are done.
