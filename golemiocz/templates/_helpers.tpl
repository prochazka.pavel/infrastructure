{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "golemiocz.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "golemiocz.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "golemiocz.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "golemiocz.selectorLabels" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "golemiocz.labels" -}}
{{ include "golemiocz.selectorLabels" . }}
helm.sh/chart: {{ include "golemiocz.chart" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/part-of: {{ include "golemiocz.name" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- end -}}

{{/*
Common annotations
*/}}
{{- define "golemiocz.annotations" -}}
{{- end -}}

{{/*
Custom helpers
*/}}

{{- define "golemiocz.imagePullSecrets" -}}
  {{- $top := index . 0 -}}
  {{- $pullSecrets := index . 1 -}}

  {{- if (not (empty $pullSecrets)) }}
imagePullSecrets:
    {{- range $pullSecrets | uniq }}
  - name: {{ . }}
    {{- end }}
  {{- end }}
{{- end -}}

{{- define "golemiocz.imageName" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{ index $top "Values" "global" "imageRegistry" }}/{{ index $top "Values" $appSection "repository" }}{{ if index $top "Values" $appSection "branch" }}/{{ index $top "Values" $appSection "branch" }}{{- end -}}:{{ index $top "Values" $appSection "tag" }}
{{- end -}}

{{- define "golemiocz.maxOldSpaceSize" -}}
{{- $memoryLimit := regexFind "[0-9]+" . }}
{{- $reserveCoeficient := 0.25 }}
{{- $memoryReserve := max 64 (min 512 (mulf $reserveCoeficient $memoryLimit)) }}
{{- printf "--max-old-space-size=%d" (subf $memoryLimit $memoryReserve | int) -}}
{{- end -}}

{{- define "golemiocz.rollingUpdate" -}}
rollingUpdate:
  maxUnavailable: {{ if gt (index . "replicas" | toDecimal) 1 -}}1{{- else -}}0{{- end }}
  maxSurge: 1
{{- end -}}
