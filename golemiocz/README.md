
# Rabin
```bash
helm upgrade --install -n frontend --values ./golemiocz/values-development-versions.yaml --values cluster_development/frontend_golemiocz-values.yaml golemiocz ./golemiocz
```

# Golem
```bash
helm upgrade --install -n frontend --values ./golemiocz/values-master-versions.yaml --values cluster_production/frontend_golemiocz-values.yaml golemiocz ./golemiocz
```
