# Rabin
```bash
helm upgrade --install -n frontend --values ./pcdc/values-development-versions.yaml --values cluster_development/frontend_pcdc-values.yaml pcdc ./pcdc
```
# Golem
```bash
helm upgrade --install -n frontend --values ./pcdc/values-master-versions.yaml --values cluster_production/frontend_pcdc-values.yaml pcdc ./pcdc
```
# Rabin - 2021
```bash
helm upgrade --install -n frontend --values cluster_development/frontend_pcdc2021-values.yaml pcdc2021 ./pcdc
```
# Golem - 2021
```bash
helm upgrade --install -n frontend --values cluster_production/frontend_pcdc2021-values.yaml pcdc2021 ./pcdc
```
