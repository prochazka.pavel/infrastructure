# Rabín
helm upgrade --install --namespace monitoring --values ../rabin/monitoring/values-secret.yaml monitoring-secret monitoring-secret

# Golem
helm upgrade --install --namespace monitoring --values ../golem/monitoring/values-secret.yaml monitoring-secret monitoring-secret
