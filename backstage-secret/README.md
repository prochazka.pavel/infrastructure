helm upgrade --install --namespace backstage --values ../myfolder/values.yml backstage-secret .


# Rabin
```bash
helm upgrade --install --values ../rabin/backstage/values-secret.yaml -n backstage backstage-secret ./backstage-secret
```

# Golem
```bash
helm upgrade --values ../golem/backstage/values-secret.yaml -n backstage backstage-secret ./backstage-secret
```
