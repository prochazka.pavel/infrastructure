
## Rabin
```bash
helm upgrade --install --namespace lkod --values ../rabin/lkod/values-secret.yaml  lkod-secret ./lkod-secret
```

## Golem
```bash
helm upgrade --install --namespace lkod --values ../golem/lkod/values-secret.yaml lkod-secret ./lkod-secret
```


## Golem - DEMO
```bash
helm upgrade --install --namespace lkod-demo --values ../golem/lkod-demo/values-secret.yaml lkod-demo-secret ./lkod-secret
```
