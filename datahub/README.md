# DataHub Kubernetes

## Problémy k dořešení

Zjištěné problémy, které bude nutno dořešit. Seřazeno dle priorit, jak blokují pokračování v další analýze. Detailní popis chyb a použitých příkazů zaslán emailem na lmaterna@totalservice.cz .

RBAC konfigurace vytvořena pouze na DEV serveru, TEST servery nebyly nijak analyzovány.

1. DNS

- kubernetes nemá přístup na DNS servery MHPMP, coreDNS nenaběhne
- DEV VM nejsou zavedené v DNS doméně, nefunguje nslookup mezi jednotlivými node

2. kubernetes networking

chybí popis síťové infrastrukty, kde je Edge router, load balancer na vstupu ingressu

3. kubernetes storage

shared storage pro uložení konfigurací

## MHMP VPN

Návod na připojení do MHPM:

(https://operator.sharepoint.com/:w:/r/_layouts/15/Doc.aspx?sourcedoc=%7B5BA8039F-E635-4C66-8505-865534DD461B%7D&file=VPN%20na%20MHMP.docx&action=default&mobileredirect=true&DefaultItemOpen=1)

https://ma.praha-mesto.cz

- username: srba.jiri@operatorict.cz
- password: RSA token id

## HOP server

Přístup testován přes HOP server: `hs-oict04.mag.mepnet.cz`

Poznámka Jidra Báča: OICT má 9 HOP serverů, prvni tri primarne pouzivame z provozu, 5 a 6 tusim databazisti, takže nejlepe použít 7,8 nebo 9

Remote desktop – HOP server beží na OS Windows, přístup je přes protocol RDP

Username: `NT\m000xe00xxxx`

**Pozor, platnost hesla** tohoto uživatele je standartně nastavena na 90 dní bez zaslání jakékoli notifikace nebo varování, tudíž je potřeba si expiraci ohlídat příkazem

## Kubernetes DEV servery

2x3 VM rozlišené dle konvence

- `w`: worker node
- `m`: master node

```
srvdhk8swdev01 10.125.162.90
srvdhk8swdev02 10.125.162.91
srvdhk8swdev03 10.125.162.92
srvdhk8smdev01 10.125.162.93
srvdhk8smdev02 10.125.162.94
srvdhk8smdev03 10.125.162.95
```

## Přístupy na DEV servery

ssh username: `oictk8s`

Pod tímto účtem `oictk8s` je vytvořený i kubeconfig pro ovládání API kubernetu.

```
openssl req -new -newkey rsa:4096 -nodes -keyout oictk8s.key -out oictk8s.csr -subj "/CN=oictk8s/O=oict-admins"
```

```
user=oictk8s

cluster=k8sdev01
```

https://gitlab.com/operator-ict/devops/kubernetes#rbac-users

## Administrator kubeconfig

Administrator konfigurace vytvořená z instalaci clusteru přes kubeadm je uložena na master node v adresáři

`/etc/kubernetes/admin.conf`

Pro její použití stačí přesměrovat přes ENV proměnnou:

`export KUBECONFIG=/etc/kubernetes/admin.conf`

Pak je možné kubernetes cluster ovládat pod uživatelem `root` přes `kubectl` příkazy. Nepoužívat pro běžnou práci, k tomu jsou další RBAC role.

## RBAC

Kube config s admin rolí je vytvořena a uložena

- Linux username: oictk8s
- Kubernetes uzivatel: oictk8s
- worker node: srvdhk8swdev01

Vytvořeny Cluster Role s odpovídajícím přiřazením cluster role bindingu dle stávající koncepce na geetoo clusteru:

- oict-admins
- developer

https://gitlab.com/operator-ict/devops/kubernetes/-/blob/master/RBAC/oict-rbac-clusterole.yaml

## Gitlab agent

Gitlab agenta nebylo možné zprovonit diký nefukčnímu DNS, takže pouze popis pro instalaci

Testovací gitlab repository: https://gitlab.com/operator-ict/golemio/devops/gitlab-agent

Instalace do nově vytvořeného namespace `gitlab-kubernetes-agent`:

```
docker run --pull=always --rm \
  registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate \
  --agent-token=gYjMVV_azGVWJ-RxzEqYgVstFS1groqKmb85fCwS2b6UJwzyyQ \
  --kas-address=wss://kas.gitlab.com \
  --agent-version stable \
  --namespace gitlab-kubernetes-agent | kubectl apply -f -
```

Problémy - nefunguje spojení na gitlab.com, nefunkční DNS:

```
{"level":"error","time":"2021-11-02T14:36:22.042Z","msg":"Error handling a connection","mod_name":"reverse_tunnel","error":"Connect(): rpc error: code = Unavailable desc = connection error: desc = \"transport: Error while dialing failed to WebSocket dial: failed to send handshake request: Get \\\"https://kas.gitlab.com\\\": context deadline exceeded\""}
```
