# Rabin
```bash
helm upgrade --install -n permission-services --values ../rabin/permission-services/values-secret.yaml permission-services-secret ./permission-services-secret
```

# Golem
```bash
helm upgrade --install -n permission-services --values ../golem/permission-services/values-secret.yaml permission-services-secret ./permission-services-secret
```
