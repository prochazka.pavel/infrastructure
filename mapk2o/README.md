
# Rabin
```bash
helm upgrade --install -n mapk2o --values ./mapk2o/values-development-versions.yaml --values ./cluster_development/mapk2o_mapk2o-values.yaml mapk2o ./mapk2o
helm upgrade --install -n mapk2o --values ./mapk2o/values-development-versions.yaml --values ./cluster_development/mapk2o_golemio-values.yaml mapk2o-golemio ./golemio
```

# Golem
```bash
helm upgrade --install -n mapk2o --values ./mapk2o/values-master-versions.yaml --values ./cluster_production/mapk2o_mapk2o-values.yaml mapk2o ./mapk2o
helm upgrade --install -n mapk2o --values ./mapk2o/values-master-versions.yaml --values ./cluster_production/mapk2o_golemio-values.yaml mapk2o-golemio ./golemio
```

# Minio
Connect to pod on admin port 9000  
login using *minio* secret credentials  
In minio admin website create Access key "QEktHXYlUahcWsFiACFv" and Secret key from [secret](https://gitlab.com/operator-ict/security/golemio-rabin/-/blob/master/mapk2o/values-secret.yaml)  
And set policy:  
```
{
 "Version": "2012-10-17",
 "Statement": [
  {
   "Effect": "Allow",
   "Action": [
    "s3:*"
   ],
   "Resource": [
    "arn:aws:s3:::*"
   ]
  },
  {
   "Effect": "Deny",
   "Action": [
    "s3:*"
   ],
   "Resource": [
    "arn:aws:s3:::*"
   ]
  }
 ]
}
```
