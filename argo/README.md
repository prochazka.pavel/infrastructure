# Argo

## Deploy configurations:

Deploy configs Rabin:
```
helm upgrade --install -n argo --values ../rabin/argo/values-secret.yaml argo-secret ./argo-secret
helm upgrade --install --values ./argo/values-development-versions.yaml --values cluster_development/argo_argo-values.yaml argo ./argo
```

Deploy configs Golem:
```
helm upgrade --install -n argo --values ../golem/argo/values-secret.yaml argo-secret ./argo-secret 
helm upgrade --install --values ./argo/values-master-versions.yaml --values cluster_production/argo_argo-values.yaml argo ./argo 
```


**When creating templates, all argo's (non-Helm) templating has to be escaped as in example:** `{{ "{{" }}workflow.name{{ "}}" }}`

## Modules

### Importers

- Python data imports and transformations on schedule, check the repository for details about each job.
- https://gitlab.com/operator-ict/golemio/extras/importers/

### Transformations
- DBT database transformations on schedule, check the repository for details about transformation models.
- https://gitlab.com/operator-ict/golemio/extras/transformations



