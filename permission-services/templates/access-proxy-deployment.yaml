{{ $appSection := "accessProxy" -}}
{{ $appName := (index . "Values" $appSection "name") -}}
{{- if index . "Values" $appSection "enabled" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $appName }}
  labels:
    app.kubernetes.io/name: {{ $appName }}
    {{- include "permission-proxy.labels" . | nindent 4 }}
  annotations:
    {{- include "permission-proxy.annotations" . | nindent 4 }}
spec:
  replicas: {{ index .Values $appSection "replicas" }}
  strategy: {{- include "permission-proxy.rollingUpdate" (index . "Values" $appSection) | nindent 4 }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ $appName }}
      {{- include "permission-proxy.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ $appName }}
        {{- include "permission-proxy.labels" . | nindent 8 }}
      annotations:
        {{- include "permission-proxy.annotations" . | nindent 8 }}
    spec:
      automountServiceAccountToken: false
      {{- include "permission-proxy.imagePullSecrets" (list . .Values.global.imagePullSecrets ) | nindent 6 }}
      containers:
        - name: {{ $appName }}
          image: {{ include "permission-proxy.imageName" (list . $appSection) | quote }}
          imagePullPolicy: {{ .Values.global.pullPolicy }}
          env:
            - name: APP_NAME
              value: {{ index .Values $appSection "name" }}
            - name: DATABASE_HOST
              value: localhost
            - name: LOG_LEVEL
              value: {{ index .Values $appSection "logLevel" | quote}}
            - name: NODE_ENV
              value: production
            - name: NODE_OPTIONS
              value: {{ include "permission-proxy.maxOldSpaceSize" (index . "Values" $appSection "resources" "limits" "memory" ) | quote}}
            - name: PORT
              value: {{ index .Values $appSection "port" | quote }}
            - name: PROXY_PAYLOAD_LIMIT
              value: {{ printf "%smb" (index .Values $appSection "maxBodySizeMB" | toString) | quote }}
            - name: POSTGRES_CONN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: accessProxyPostgresConn
            - name: POSTGRES_POOL_MAX_CONNECTIONS
              value: "10"
            - name: PGSSLMODE
              value: {{ index .Values $appSection "sslMode" | quote }}
            - name: REDIS_CONN
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: redis-conn
            - name: REDIS_ENABLE
              value: "true"
            - name: REDIS_TTL
              value: "43200"
            - name: TOKEN_ALGORITHM
              value: HS256
            - name: TOKEN_HAS_CERT
              value: "false"
            - name: TOKEN_ISSUER
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: tokenIssuer
            - name: TOKEN_SECRET_OR_CERT_PATH
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: tokenSecretOrCertPath
            {{- if index . "Values" $appSection "inspector" "enabled" }}
            - name: INSPECTOR_HOST
              value: {{ index . "Values" $appSection "inspector" "host" | quote }}
            {{- end }}
            - name: JAEGER_ENABLED
              value: {{ index .Values $appSection "opentelemetry" "enabled" | quote }}
            {{- if index .Values $appSection "opentelemetry" "enabled" }}
            - name: JAEGER_PROCESSOR
              value: {{ index .Values.opentelemetry.processor | upper | quote }}
            - name: JAEGER_AGENT_HOST
              value: {{ index .Values.opentelemetry.host | quote }}
            - name: JAEGER_AGENT_PORT
              value: {{ index .Values.opentelemetry.port | quote }}
            - name: OTEL_TRACES_SAMPLER
              value: {{ index .Values.opentelemetry.sampler | quote }}
            - name: OTEL_TRACES_SAMPLER_ARG
              value: {{ index .Values $appSection "opentelemetry" "samplerArg" | quote }}
            - name: TELEMETRY_LOG_LEVEL
              value: {{ index .Values.opentelemetry.logLevel | quote }}
            {{- end }}
            {{- if (index .Values $appSection "lightship" "handlerTimeout") }}
            - name: LIGHTSHIP_HANDLER_TIMEOUT
              value: {{ index . "Values" $appSection "lightship" "handlerTimeout" | quote }}
            {{- end }}
            {{- if (index .Values $appSection "lightship" "shutdownDelay") }}
            - name: LIGHTSHIP_SHUTDOWN_DELAY
              value: {{ index . "Values" $appSection "lightship" "shutdownDelay" | quote }}
            {{- end }}
            {{- if (index .Values $appSection "lightship" "shutdownTimeout") }}
            - name: LIGHTSHIP_SHUTDOWN_TIMEOUT
              value: {{ index . "Values" $appSection "lightship" "shutdownTimeout" | quote }}
            {{- end }}
            - name: ROARR_LOG
              value: "false"
          ports:
          - containerPort: {{ index . "Values" $appSection "port" }}
          {{- if index . "Values" $appSection "inspector" "enabled" }}
          - containerPort: {{ index . "Values" $appSection "inspector" "port" }}
          {{- end }}
          resources:
            {{ index .Values $appSection "resources" | toYaml | nindent 12 }}
          securityContext:
            {{- include "debug-mode" (index . "Values" $appSection) | nindent 12 }}
          {{- if index . "Values" $appSection "probes" "enabled" }}
          readinessProbe: {{ index .Values $appSection "readinessProbe" | toYaml | nindent 12 }}
          livenessProbe: {{ index .Values $appSection "livenessProbe" | toYaml | nindent 12 }}
          {{ end }}
{{ end }}
