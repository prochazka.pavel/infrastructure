# Permission services

# Rabin
```bash
helm upgrade -n permission-services --install --values ./permission-services/values-development-versions.yaml --values cluster_development/permission-services_permission-services-values.yaml permission-services ./permission-services
```

# Golem
```bash
helm upgrade -n permission-services --install --values ./permission-services/values-master-versions.yaml --values cluster_production/permission-services_permission-services-values.yaml permission-services ./permission-services
```
