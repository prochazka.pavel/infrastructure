helm upgrade --install --namespace golemio --values ../myfolder/values.yml golemio-secret .


# Rabin
```bash
helm upgrade --install --values ../rabin/golemio/values-secret.yaml -n golemio golemio-secret ./golemio-secret
```

## Rabin / mapk2o
```bash
helm upgrade --install --values ../rabin/mapk2o-golemio/values-secret.yaml -n mapk2o golemio-secret ./golemio-secret
```

## Rabin - VP
```bash
helm upgrade --install --values ../rabin/vehicle-positions/values-secret.yaml -n vehicle-positions golemio-secret ./golemio-secret
```


# Golem
```bash
helm upgrade --values ../golem/golemio/values-secret.yaml -n golemio golemio-secret ./golemio-secret
```

## Golem - VP
```bash
helm upgrade --values ../golem/vehicle-positions/values-secret.yaml -n vehicle-positions golemio-secret ./golemio-secret
```
