#!/bin/bash

# https://stackoverflow.com/a/53175209/1820591

function encode_password()
{
    SALT=$(od -A n -t x -N 4 /dev/urandom)
    PASS=$SALT$(echo -n $1 | xxd -ps | tr -d '\n' | tr -d ' ')
    PASS=$(echo -n $PASS | xxd -r -p | sha256sum | head -c 128)
    PASS=$(echo -n $SALT$PASS | xxd -r -p | base64 | tr -d '\n')
    echo $PASS
}

[ "x$1" == "x-p" -a "x$2" != "x" ] || { echo "Usage: $0 -p <password>" ; exit 1 ; }
encode_password "$2"
