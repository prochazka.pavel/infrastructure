# Rabín
helm upgrade --install --namespace queue queue-secret queue-secret --values ../rabin/queue/values-secret.yaml

# Golem
helm upgrade --install --namespace queue queue-secret queue-secret --values ../golem/queue/values-secret.yaml


# Parking - dev 
```bash
helm upgrade --install --namespace parking-dev queue-secret queue-secret --values ../rabin/parking-dev/values-secret.yaml
```

# Parking - prod
```bash
helm upgrade --install --namespace parking queue-secret queue-secret --values ../golem/parking/values-secret.yaml
```
