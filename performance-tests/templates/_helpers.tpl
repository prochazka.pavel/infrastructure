{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "performance-tests.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "performance-tests.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "performance-tests.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "performance-tests.selectorLabels" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "performance-tests.labels" -}}
{{ include "performance-tests.selectorLabels" . }}
helm.sh/chart: {{ include "performance-tests.chart" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/part-of: {{ include "performance-tests.name" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- end -}}

{{/*
Common annotations
*/}}
{{- define "performance-tests.annotations" -}}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "performance-tests.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "performance-tests.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}


{{/*
Custom helpers
*/}}

{{- define "performance-tests.imageName" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{ index $top "Values" "global" "imageRegistry" }}/{{ index $top "Values" $appSection "repository" }}{{ if index $top "Values" $appSection "branch" }}/{{ index $top "Values" $appSection "branch" }}{{- end -}}:{{ index $top "Values" $appSection "tag" }}
{{- end -}}

{{- define "performance-tests.readinessProbe" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{- $ingress := index $top "Values" $appSection "traefik" | default dict -}}
httpGet:
  path: {{ index $ingress "path" | default "/" | quote }}
  port: {{ index $top "Values" $appSection "port" }}
initialDelaySeconds: {{ index $top "Values" "global" "readinessProbe" "initialDelaySeconds" }}
periodSeconds: {{ index $top "Values" "global" "readinessProbe" "periodSeconds" }}
{{- end -}}

{{- define "performance-tests.livenessProbe" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{- $ingress := index $top "Values" $appSection "traefik" | default dict -}}
httpGet:
  path: {{ index $ingress "path" | default "/" | quote }}
  port: {{ index $top "Values" $appSection "port" }}
{{- end -}}
