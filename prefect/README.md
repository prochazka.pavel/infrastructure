# Prefect

https://github.com/PrefectHQ/prefect-helm/tree/main/charts/prefect-server
https://github.com/PrefectHQ/prefect-helm/tree/main/charts/prefect-worker

```bash
helm repo add prefect https://prefecthq.github.io/prefect-helm

```

# Rabin
```bash
# Server
helm upgrade --install -n prefect --values cluster_development/prefect_server-values.yaml prefect-server prefect/prefect-server

# Worker
helm upgrade --install -n prefect --values cluster_development/prefect_worker-values.yaml  prefect-worker prefect/prefect-worker
```