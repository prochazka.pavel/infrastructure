# LKOD

https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/blob/master/demo/README.md

# Rabin
```bash
helm upgrade --install -n lkod --values ./lkod/values-development-versions.yaml --values cluster_development/lkod_lkod-values.yaml lkod ./lkod
```

# Golem
```bash
helm upgrade --install -n lkod --values ./lkod/values-master-versions.yaml --values cluster_production/lkod_lkod-values.yaml lkod ./lkod
```
## Golem - DEMO
```bash
helm upgrade --install -n lkod-demo --values ./lkod/values-master-versions.yaml --values cluster_production/lkod_lkod-demo-values.yaml lkod-demo ./lkod
```
