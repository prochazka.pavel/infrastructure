{{ $appSection := "cms-v2" -}}
{{ $appName := (index . "Values" $appSection "name") -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $appName }}
  annotations:
    rollme: {{ .Values.configurationFiles.routeDefinitions | toYaml | sha256sum | trunc 8 | quote }}
    {{- include "pragozor.annotations" . | nindent 4 }}
  labels:
    app.kubernetes.io/name: {{ $appName }}
    {{- include "pragozor.labels" . | nindent 4 }}
spec:
  replicas: {{ index . "Values" $appSection "replicas" }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ $appName }}
      {{- include "pragozor.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/" $appName "-configmap.yaml") . | sha256sum }}
        {{- include "pragozor.annotations" . | nindent 8 }}
      labels:
        app.kubernetes.io/name: {{ $appName }}
        {{- include "pragozor.labels" . | nindent 8 }}
    spec:
      containers:
        - name: {{ $appName }}
          image: {{ include "pragozor.imageName" (list . $appSection) | quote }}
          imagePullPolicy: {{ .Values.global.pullPolicy }}
          env:
            - name: API_TOKEN_SALT
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: api-token-salt
            - name: ADMIN_JWT_SECRET
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: admin-jwt-secret
            - name: JWT_SECRET
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: jwt-secret
            - name: DATABASE_HOST
              value: {{ .Values.postgres.hostname | quote }}
            - name: DATABASE_PORT
              value: {{ .Values.postgres.port | quote }}
            - name: DATABASE_NAME
              value: {{ .Values.postgres.database | quote }}
            - name: DATABASE_USERNAME
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: postgres-username
            - name: DATABASE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.global.secretName }}
                  key: postgres-password
            - name: DATABASE_SSL
              value: {{ if .Values.postgres.ssl }}"true"{{ else }}"false"{{ end }}
          ports:
            - containerPort: {{ index . "Values" $appSection "port" }}
          resources:
            {{ index . "Values" $appSection "resources" | toYaml | nindent 12 }}
          securityContext:
            capabilities:
              drop: [ "all" ]
          {{- if index . "Values" $appSection "probes" "enabled" }}
          readinessProbe: {{include "pragozor.readinessProbe" (list . $appSection) | nindent 12 }}
          livenessProbe: {{include "pragozor.livenessProbe" (list . $appSection) | nindent 12 }}
          {{ end }}
          volumeMounts:
            - name: {{ $appName }}-config
              mountPath: /home/node/app/config/routeDefinitions.json
              subPath: routeDefinitions.json
              readOnly: true
      serviceAccountName: {{ include "pragozor.serviceAccountName" . }}
      volumes:
        - name: {{ $appName }}-config
          configMap:
            name: {{ $appName }}-config
            items:
              - key: routeDefinitions.json
                path: routeDefinitions.json
