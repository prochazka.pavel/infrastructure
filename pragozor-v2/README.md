# Rabin
```bash
helm upgrade --install -n pragozor --values ./pragozor-v2/values-development-versions.yaml --values cluster_development/pragozor_pragozor-v2-values.yaml pragozor-v2 ./pragozor-v2
```

# Golem
```bash
helm upgrade --install -n pragozor --values ./pragozor-v2/values-master-versions.yaml --values cluster_production/pragozor_pragozor-v2-values.yaml pragozor-v2 ./pragozor-v2
```
