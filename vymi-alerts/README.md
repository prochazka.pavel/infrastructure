
# Rabin
```bash
helm upgrade --install -n jis --values ./vymi-alerts/values-development-versions.yaml --values ./cluster_development/jis_vymi-alerts-values.yaml vymi-alerts ./vymi-alerts
```

# Golem
```bash
helm upgrade --install -n jis --values ./vymi-alerts/values-main-versions.yaml --values ./cluster_production/jis_vymi-alerts-values.yaml vymi-alerts ./vymi-alerts
```
