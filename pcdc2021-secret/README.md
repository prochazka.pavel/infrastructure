# Rabin
```bash
helm upgrade --install --namespace frontend --values ../rabin/pcdc2021/values-secret.yaml pcdc2021-secret pcdc2021-secret
```

# Golem
```bash
helm upgrade --install  --namespace frontend --values ../golem/pcdc2021/values-secret.yaml pcdc2021-secret pcdc2021-secret
```
