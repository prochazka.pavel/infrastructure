{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "permission-proxy.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "permission-proxy.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "permission-proxy.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "permission-proxy.selectorLabels" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ include "permission-proxy.name" . }}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "permission-proxy.labels" -}}
{{ include "permission-proxy.selectorLabels" . }}
helm.sh/chart: {{ include "permission-proxy.chart" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- end -}}

{{/*
Common annotations
*/}}
{{- define "permission-proxy.annotations" -}}
{{- end -}}

{{/*
Custom helpers
*/}}

{{- define "permission-proxy.imagePullSecrets" -}}
  {{- $top := index . 0 -}}
  {{- $pullSecrets := index . 1 -}}

  {{- if (not (empty $pullSecrets)) }}
imagePullSecrets:
    {{- range $pullSecrets | uniq }}
  - name: {{ . }}
    {{- end }}
  {{- end }}
{{- end -}}

{{- define "permission-proxy.imageName" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{ index $top "Values" "global" "imageRegistry" }}/{{ index $top "Values" $appSection "repository" }}{{ if index $top "Values" $appSection "branch" }}/{{ index $top "Values" $appSection "branch" }}{{- end -}}:{{ index $top "Values" $appSection "tag" }}
{{- end -}}

{{- define "permission-proxy.readinessProbe" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
httpGet:
  path: /ready
  port: 9000
initialDelaySeconds: {{ index $top "Values" "global" "readinessProbe" "initialDelaySeconds" }}
periodSeconds: {{ index $top "Values" "global" "readinessProbe" "periodSeconds" }}
{{- end -}}

{{- define "permission-proxy.livenessProbe" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
httpGet:
  path: /live
  port: 9000
{{- end -}}

{{- define "permission-proxy.readinessProbeUI" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{- $ingress := index $top "Values" $appSection "traefik" | default dict -}}
httpGet:
  path: {{ index $ingress "path" | default "/" | quote }}
  port: {{ index $top "Values" $appSection "port" }}
initialDelaySeconds: {{ index $top "Values" "global" "readinessProbe" "initialDelaySeconds" }}
periodSeconds: {{ index $top "Values" "global" "readinessProbe" "periodSeconds" }}
{{- end -}}

{{- define "permission-proxy.livenessProbeUI" -}}
{{- $top := index . 0 -}}
{{- $appSection := index . 1 -}}
{{- $ingress := index $top "Values" $appSection "traefik" | default dict -}}
httpGet:
  path: {{ index $ingress "path" | default "/" | quote }}
  port: {{ index $top "Values" $appSection "port" }}
{{- end -}}

{{- define "permission-proxy.rollingUpdate" -}}
rollingUpdate:
  maxUnavailable: {{ if gt (index . "replicas" | toDecimal) 1 -}}1{{- else -}}0{{- end }}
  maxSurge: 1
{{- end -}}

{{- define "permission-proxy.maxOldSpaceSize" -}}
{{- $memoryLimit := regexFind "[0-9]+" . }}
{{- $reserveCoeficient := 0.25 }}
{{- $memoryReserve := max 64 (min 512 (mulf $reserveCoeficient $memoryLimit)) }}
{{- printf "--max-old-space-size=%d" (subf $memoryLimit $memoryReserve | int) -}}
{{- end -}}
