# Permission proxy

# Rabin
```bash
helm upgrade -n golemio --install --values ./permission-proxy/values-development-versions.yaml --values cluster_development/golemio_permission-proxy-values.yaml permission-proxy ./permission-proxy
```

# Golem
```bash
helm upgrade -n golemio --install --values ./permission-proxy/values-master-versions.yaml --values cluster_production/golemio_permission-proxy-values.yaml permission-proxy ./permission-proxy
```
