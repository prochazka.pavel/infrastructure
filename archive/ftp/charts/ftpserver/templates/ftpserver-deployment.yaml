{{ $appName := .Values.name -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $appName }}
  annotations:
    {{- include "ftp.annotations" . | nindent 4 }}
  labels:
    app.kubernetes.io/name: {{ $appName }}
    {{- include "ftp.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicas }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ $appName }}
  template:
    metadata:
      annotations:
        {{- include "ftp.annotations" . | nindent 8 }}
      labels:
        app.kubernetes.io/name: {{ $appName }}
        {{- include "ftp.labels" . | nindent 8 }}
    spec:
      serviceAccountName: {{ include "ftp.serviceAccountName" . }}
      initContainers:
        # Create FTP certificate bundle from ingress certificate and key.
        - name: {{ $appName }}-init
          image: busybox
          command: ["/bin/sh"]
          args:
            - "-c"
            - "echo \"`date -Iseconds` Creating the probe script...\" ; cat /data/chkcert.tpl > /data/chkcert.sh ; chmod 755 /data/chkcert.sh ; echo \"`date -Iseconds` Concatenating /data/tls/tls.crt and /data/tls/tls.key to /data/pureftpd.pem...\" ; cat /data/tls/tls.crt /data/tls/tls.key > /data/pureftpd.pem"
          resources:
            limits:
              cpu: 50m
              memory: 50Mi
            requests:
              cpu: 10m
              memory: 20Mi
          volumeMounts:
            - name: configdata
              mountPath: "/data"
            - name: ftp-config
              mountPath: "/data/chkcert.tpl"
              subPath: chkcert.tpl
              readOnly: true
            - name: tls-secret
              mountPath: "/data/tls"
              readOnly: true
      containers:
        - name: {{ $appName }}
          image: {{ printf "%s:%s" .Values.image .Values.tag | quote }}
          imagePullPolicy: {{ .Values.global.pullPolicy }}
          ports:
            - containerPort: {{ .Values.port }}
          env:
            - name: TZ
              value: "Europe/Prague"
            - name: AUTH_METHOD
              value: "pgsql"
            - name: SECURE_MODE
              # https://github.com/crazy-max/docker-pure-ftpd/blob/8f4dfaa891769dd891385c078a0ffd07898c1e90/entrypoint.sh#L43-L55*/}}
              value: "false"
            - name: PASSIVE_PORT_RANGE
              value: "{{ .Values.passivePortStart }}:{{ .Values.passivePortEnd }}"
          livenessProbe:
            exec:
              command:
                - /bin/sh
                - -c
                - /data/chkcert.sh
            # Liveness probe is used for FTPS certificate reloading.
            failureThreshold: 1
            timeoutSeconds: 5
          resources:
            {{ .Values.resources | toYaml | nindent 12 }}
          volumeMounts:
            - name: configdata
              mountPath: "/data"
            - name: ftpstorage
              mountPath: "/home"
            - name: ftp-secret
              mountPath: "/data/pureftpd-pgsql.conf"
              subPath: pureftpd.conf
              readOnly: true
            - name: ftp-config
              mountPath: "/data/pureftpd.flags"
              subPath: pureftpd.flags
              readOnly: true
            - name: ftp-secret
              mountPath: "/data/pureftpd-dhparams.pem"
              subPath: pureftpd-dhparams.pem
              readOnly: true
            - name: tls-secret
              mountPath: "/data/tls"
              readOnly: true
      volumes:
        - name: configdata
          emptyDir: {}
        - name: ftpstorage
          persistentVolumeClaim:
            claimName: {{ .Values.global.pvcName }}-pvc
        - name: ftp-secret
          secret:
            secretName: ftp-secret
            items:
              - key: pureftpd-pgsql.conf
                path: pureftpd.conf
              - key: dhparams.pem
                path: pureftpd-dhparams.pem
        - name: tls-secret
          secret:
            secretName: {{ .Values.global.ingress.host }}-tls
        - name: ftp-config
          configMap:
            name: {{ $appName }}-config
            items:
              - key: pureftpd.flags
                path: pureftpd.flags
              - key: chkcert.tpl
                path: chkcert.tpl
