{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "ftp.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "ftp.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ftp.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "ftp.selectorLabels" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "ftp.labels" -}}
{{ include "ftp.selectorLabels" . }}
helm.sh/chart: {{ include "ftp.chart" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/part-of: {{ include "ftp.name" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- end -}}

{{/*
Common annotations
*/}}
{{- define "ftp.annotations" -}}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "ftp.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "ftp.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}


{{/*
Custom helpers
*/}}

{{- define "ftp.imageName" -}}
{{- $top := index . 0 -}}
{{ index $top "Values" "global" "imageRegistry" }}/{{ index $top "Values" "repository" }}{{ if index $top "Values" "branch" }}/{{ index $top "Values" "branch" }}{{- end -}}:{{ index $top "Values" "tag" }}
{{- end -}}

{{- define "ftp.readinessProbe" -}}
{{- $top := index . 0 -}}
{{- $ingress := index $top "Values" "global" "ingress" | default dict -}}
httpGet:
  path: {{ index $ingress "path" | default "/" | quote }}
  port: {{ index $top "Values" "port" }}
initialDelaySeconds: {{ index $top "Values" "global" "readinessProbe" "initialDelaySeconds" }}
periodSeconds: {{ index $top "Values" "global" "readinessProbe" "periodSeconds" }}
{{- end -}}

{{- define "ftp.livenessProbe" -}}
{{- $top := index . 0 -}}
{{- $ingress := index $top "Values" "global" "ingress" | default dict -}}
httpGet:
  path: {{ index $ingress "path" | default "/" | quote }}
  port: {{ index $top "Values" "port" }}
{{- end -}}
