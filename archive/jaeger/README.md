# Jaeger

## Sampling info
- https://github.com/open-telemetry/opentelemetry-js/blob/main/packages/opentelemetry-core/src/utils/sampling.ts#L17
- https://www.aspecto.io/blog/opentelemetry-sampling-everything-you-need-to-know/
- *Node.js implementace není aktuálně schopná převzít sampling strategy od Agenta*

## Nekompatibilita s Elastic 8

Pro přípravu Elasticu je potřeba obejít nekompatibilitu Jaeger s Elastic 8 dle 
https://github.com/jaegertracing/jaeger/issues/3571#issuecomment-1124926679 a
https://github.com/jaegertracing/jaeger/issues/3571#issuecomment-1141921325

a odebrat protože jinak to neprojde
```json
"_size": {
"enabled": false
},
```

## Check sampling strategy on agent, which is setup on collector
```bash
kubectl port-forward jaeger-agent-<POD_NAME> 5778:5778

curl http://localhost:5778/sampling?service=<SERVICE_NAME>
```

# Instalace přes helm chart
https://github.com/jaegertracing/helm-charts/tree/main/charts/jaeger

```bash
helm repo add jaegertracing https://jaegertracing.github.io/helm-charts

kubectl create secret generic jaeger-secret --from-literal=ES_USERNAME=elastic --from-literal=ES_PASSWORD=S6Pgvq7svin3FJ705t8P68w0 -n observability

#dev
helm upgrade --install jaeger jaegertracing/jaeger -n observability --values jaeger/dev.yaml --version 0.71.4

#prod
helm upgrade --install jaeger jaegertracing/jaeger -n observability --values jaeger/prod.yaml --version 0.71.4
```

# Operator (DEPRECATED)
- má složitejší nastavení a není důvod ho aktuálně používat (12/2022), tedy přecházíme zpět na Helm
- CRD vyžaduje přimo namespace observability
- nejde vypnout spark a jen to padá
- instalace pak není přes ENV proměnné v aplikaci, ale přes annotaci `sidecar.jaegertracing.io/inject: "true"`, je potřeba upravit helmy

https://www.jaegertracing.io/docs/1.39/operator/

https://github.com/jaegertracing/helm-charts/blob/main/charts/jaeger-operator/values.yaml


```bash
kubectl create -f https://github.com/jaegertracing/jaeger-operator/releases/download/v1.39.0/jaeger-operator.yaml -n observability

kubectl create secret generic jaeger-secret --from-literal=ES_PASSWORD=tRtjLTAI4h770y3f444Pn12C --from-literal=ES_USERNAME=elastic  -n observability

#DEV
kaf jaeger/operator-rabin.yaml -n observability
#PROD
kaf jaeger/operator-golem.yaml -n observability
```
