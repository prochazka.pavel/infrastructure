# Launch darkly relay proxy

```bash
https://github.com/launchdarkly/ld-relay-helm

helm repo add launchdarkly-ld-relay https://launchdarkly.github.io/ld-relay-helm
```

## Dev
```bash
helm upgrade --install -n golemio ld-relay --values launchdarkly-relay-proxy/values-dev.yaml launchdarkly-ld-relay/ld-relay
```

## Prod
```bash
helm upgrade --install -n golemio ld-relay --values launchdarkly-relay-proxy/values-prod.yaml launchdarkly-ld-relay/ld-relay
```
