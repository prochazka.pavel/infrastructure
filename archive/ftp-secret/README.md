# Rabin
```bash
helm upgrade --install --values .../golemio-rabin/ftp/values-secret.yaml -n storage ftp-secret ./ftp-secret
```

# Golem
```bash
helm upgrade --install --values .../golemio-golem/ftp/values-secret.yaml -n storage ftp-secret ./ftp-secret
```
