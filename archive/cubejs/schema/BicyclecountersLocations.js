cube(`bikes_locations`, {
  sql: `SELECT id, name, vendor_id, route, lat, lng FROM bicycle_counters.bicyclecounters_locations`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    
  },
  
  measures: {
    count_loc: {
      type: `count`
    }
  },
  
  dimensions: {  
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    name: {
      sql: `name`,
      type: `string`
    },

    vendor_id: {
      sql: `vendor_id`,
      type: `string`
    },
    
    route: {
      sql: `replace(route, ' ', '')`,
      type: `string`
    },

    lat: {
      sql: `lat`,
      type: `number`
    },
    
    lng: {
      sql: `lng`,
      type: `number`
    }
  },
  
  dataSource: `default`
});
