cube(`bikes_directions`, {
  sql: `SELECT id, vendor_id, locations_id, name FROM bicycle_counters.bicyclecounters_directions`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    
  },
  
  measures: {
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    vendor_id: {
      sql: `vendor_id`,
      type: `string`
    },
    
    locations_id: {
      sql: `locations_id`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    }    
  },
  
  dataSource: `default`
});
