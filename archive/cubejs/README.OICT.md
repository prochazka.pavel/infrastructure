# Cube

# k dořešení
- udělat Minio jako remote storage?
- použít výkonější disky?premium?

Vytvoření PSQL schematu cubejs viz Ansible.

Vytvoření úložiště (v `/cube/conf/schema`):
```bash
kubectl apply -f cubejs/schema-pvc.yaml
```

Instalace Cube:
```bash
helm upgrade --install -n data-tools cubejs-secret cubejs-secret --values ../security/golemio-rabin/cubejs/values-secret.yaml
helm upgrade --install -n data-tools cubejs cubejs --values cluster_development/data-tools_cubejs-values.yaml
```

Když není zapnut development mód, není k dispozici cube.js playground.

Úpravy originálního helm chartu (přidáno):

values.yaml:
```yaml
config:
  ## The port for a Cube.js deployment to listen to PSQL connections on
  ##
  pgSqlPort:
```

templates/common-env.tpl:
```yaml
{{- if .Values.config.pgSqlPort }}
- name: CUBEJS_PG_SQL_PORT
  value: {{ .Values.config.pgSqlPort | quote }}
{{- end }}
```

templates/master/service.yaml:
```yaml
spec:
  ports:
    {{- if .Values.config.pgSqlPort }}
    - name: psql
      port: {{ .Values.config.pgSqlPort }}
      targetPort: psql
    {{- end}}
```

templates/master/deployment.yaml:
```yaml
spec:
  template:
    spec:
      containers:
        ports:
          {{- if .Values.config.pgSqlPort }}
          - name: psql
            containerPort: {{ .Values.config.pgSqlPort }}
            protocol: TCP
          {{- end }}
```
