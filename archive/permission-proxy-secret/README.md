# Rabin
```bash
helm upgrade --install -n golemio --values ../rabin/permission-proxy/values-secret.yaml permission-proxy-secret ./permission-proxy-secret
```

# Golem
```bash
helm upgrade --install -n golemio --values ../golem/permission-proxy/values-secret.yaml permission-proxy-secret ./permission-proxy-secret
```
