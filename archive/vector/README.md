# Vector

https://github.com/vectordotdev/helm-charts

https://hub.docker.com/r/timberio/vector

```bash
helm repo add vector https://helm.vector.dev
helm repo update
```

## Agent
```bash
helm upgrade --install -n monitoring --values monitoring/vector/agent/values.yaml vector-agent vector/vector --version 0.21.1
```


## Aggregator
```bash
#Rabin
helm upgrade --install -n monitoring --values monitoring/vector/aggregator/values-dev.yaml vector-aggregator vector/vector --version 0.21.1

#Golem
helm upgrade --install -n monitoring --values monitoring/vector/aggregator/values-prod.yaml vector-aggregator vector/vector --version 0.21.1
```

```mermaid
%% Soubor je potřeba editovat ručně

flowchart TB
    subgraph split_by
        nginx_tcp
        vp_ig
        permission_proxy
        nginx_vp_url_proxy
        _unmatched
    end

    nginx_tcp --> nginx_tcp_metrics
    vp_ig --> vp_parse_base_url
    vp_parse_base_url --> vp_to_metrics
    
    permission_proxy --> permission_proxy_requests_by_userId
    
    permission_proxy --> permission_proxy_parse_url
    permission_proxy_parse_url --> permission_proxy_requests_by_url
    
    nginx_vp_url_proxy --> vp_url_proxy_metric

    subgraph sinks
        LOGS
        prometheus
    end
    vp_ig --> LOGS
    _unmatched  --> LOGS
    permission_proxy_parse_url --> LOGS

    nginx_tcp_metrics --> prometheus
    vp_url_proxy_metric --> prometheus
    vp_to_metrics --> prometheus
    permission_proxy_requests_by_userId --> prometheus
    permission_proxy_requests_by_url --> prometheus

```


### Ingress
Pro přístup na Prometheus metriky z dohledového centra pro vehicle-positions je potřeba nastavit ingress. 

```bash
#Rabin
kaf monitoring/vector/aggregator/dev-prometheus-public-ingress.yaml
```