## MongoDB
https://github.com/bitnami/charts/tree/master/bitnami/mongodb

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install mongodb bitnami/mongodb

helm upgrade --install -n database mongodb bitnami/mongodb --values mongodb/mongodb-values.yaml
```
## Admin Password
```bash
export MONGODB_ROOT_PASSWORD=$(kubectl get secret --namespace database mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 -d)
```
