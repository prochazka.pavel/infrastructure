# Rabin
```bash
helm upgrade --install -n pragozor --values ./pragozor/values-development-versions.yaml --values cluster_development/pragozor_pragozor-values.yaml pragozor ./pragozor
```

# Golem
```bash
helm upgrade --install -n pragozor --values ./pragozor/values-master-versions.yaml --values cluster_production/pragozor_pragozor-values.yaml pragozor ./pragozor
```
