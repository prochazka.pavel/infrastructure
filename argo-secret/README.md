# Rabin
```bash
helm upgrade --install -n argo --values ../rabin/argo/values-secret.yaml argo-secret ./argo-secret
```

# Golem
```bash
helm upgrade --install -n argo --values ../golem/argo/values-secret.yaml argo-secret ./argo-secret
```
