# Rabin
```bash
helm upgrade --install --values ../rabin/postgres-alerts/values-secret.yaml -n golemio postgres-alerts-secret ./postgres-alerts-secret
```

# Golem
```bash
helm upgrade --install --values ../golem/postgres-alerts/values-secret.yaml -n golemio postgres-alerts-secret ./postgres-alerts-secret
```
