#!/bin/bash

case "${0}" in
  */*) scriptdir="${0%/*}" ;;
    *) scriptdir="." ;;
esac

if [ "${1}x" == "x" ]; then
    echo "${0}: Usage: ${0} <cluster name from .kube/config>" >&2
    echo "${0}: Available clusters:" >&2
    kubectx >&2
    exit 99
fi

if [ ! -r "${scriptdir}/${1}_values-ingress-nginx-internal.yaml" ]; then
    echo "${0}: File '${scriptdir}/${1}_values-ingress-nginx-internal.yaml' does not exist, aborting..." >&2
    exit 1
fi

set -e
echo -e "\033[1mSwitching to cluster context \033[7m${1}\033[0m\033[1m...\033[0m"
kubectx ${1}

echo -e "\033[1mAdding/refreshing Helm repositories...\033[0m"
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo add jetstack      https://charts.jetstack.io
helm repo update

set +e
echo -e "\033[1mCreating \033[7mingress\033[0m\033[1m namespace...\033[0m"
kubectl create namespace ingress
set -e
echo -e "\033[1mInstalling/upgrading \033[7mingress-nginx-internal\033[0m\033[1m release...\033[0m"
helm upgrade --install -n ingress ingress-nginx-internal ingress-nginx/ingress-nginx --values "${scriptdir}/${1}_values-ingress-nginx-internal.yaml" --version 4.0.17
echo -e "\033[1mInstalling/upgrading \033[7mingress-nginx\033[0m\033[1m release...\033[0m"
helm upgrade --install -n ingress ingress-nginx          ingress-nginx/ingress-nginx --values "${scriptdir}/${1}_values-ingress-nginx.yaml"          --version 4.0.17
echo -e "\033[1mInstalling/upgrading \033[7mingress-nginx-udp\033[0m\033[1m release...\033[0m"
helm upgrade --install -n ingress ingress-nginx-udp      ingress-nginx/ingress-nginx --values "${scriptdir}/${1}_values-ingress-nginx-udp.yaml"      --version 4.0.17
echo -e "\033[1mInstalling/upgrading \033[7mcert-manager\033[0m\033[1m release...\033[0m"
helm upgrade --install -n ingress cert-manager           jetstack/cert-manager       --values "${scriptdir}/values-cert-manager.yaml"           --version v1.7.1
#echo -e "\033[1mInstalling/updating cluster issuer \033[7mletsencrypt-staging\033[0m\033[1m...\033[0m"
#kubectl apply -n ingress -f "${scriptdir}/clusterissuer-letsencrypt-staging.yaml"
#echo -e "\033[1mInstalling/updating cluster issuer \033[7mletsencrypt-prod\033[0m\033[1m...\033[0m"
#kubectl apply -n ingress -f "${scriptdir}/clusterissuer-letsencrypt-prod.yaml"
echo -e "\033[1mDone.\033[0m"
