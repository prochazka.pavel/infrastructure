# Ingress NGINX

https://kubernetes.github.io/ingress-nginx/deploy/
https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/configmap/

https://github.com/kubernetes/ingress-nginx/tree/main/charts/ingress-nginx

### More ingresses 
- https://github.com/kubernetes/ingress-nginx/blob/main/docs/index.md#i-have-more-than-one-controller-running-in-my-cluster-and-i-want-to-use-the-new-api-
- https://kubernetes.github.io/ingress-nginx/user-guide/multiple-ingress/

1. Nainstalovat `ingress-nginx-secrets`
2. Nainstalovat ingress/install.sh

# Rabin
```bash
helm upgrade --install -n ingress --values "ingress/dp-rabin_values-ingress-nginx.yaml"          --version 4.10.0 ingress-nginx          ingress-nginx/ingress-nginx
helm upgrade --install -n ingress --values "ingress/dp-rabin_values-ingress-nginx-internal.yaml" --version 4.10.0 ingress-nginx-internal ingress-nginx/ingress-nginx
helm upgrade --install -n ingress --values "ingress/dp-rabin_values-ingress-nginx-local.yaml"    --version 4.10.0 ingress-nginx-local    ingress-nginx/ingress-nginx
#helm upgrade --install -n ingress --values "ingress/dp-rabin_values-ingress-nginx-udp.yaml"      --version 4.4.0 ingress-nginx-udp      ingress-nginx/ingress-nginx
```

# Golem
```bash
helm upgrade --install -n ingress --values "ingress/dp-golem_values-ingress-nginx.yaml"          --version 4.10.0 ingress-nginx          ingress-nginx/ingress-nginx
helm upgrade --install -n ingress --values "ingress/dp-golem_values-ingress-nginx-internal.yaml" --version 4.10.0 ingress-nginx-internal ingress-nginx/ingress-nginx
helm upgrade --install -n ingress --values "ingress/dp-golem_values-ingress-nginx-local.yaml"    --version 4.10.0 ingress-nginx-local    ingress-nginx/ingress-nginx
#helm upgrade --install -n ingress --values "ingress/dp-golem_values-ingress-nginx-udp.yaml"      --version 4.4.0 ingress-nginx-udp      ingress-nginx/ingress-nginx
```

# DNS Challenge
See ingress-secrets.

# Cert manager
https://github.com/cert-manager/cert-manager/tree/master/deploy/charts/cert-manager

```bash
helm search repo jetstack/cert-manager --versions

helm upgrade --install -n ingress cert-manager jetstack/cert-manager --values "ingress/values-cert-manager.yaml" --version v1.13.3
```

# Opentelemetry
List of available images https://explore.ggcr.dev/?repo=registry.k8s.io%2Fingress-nginx%2Fopentelemetry
