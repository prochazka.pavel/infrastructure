# Echo1
```bash
helm upgrade -n tmp --install --set "echo.name"="hello" echo1 ./echo
```
# Echo2
```bash
helm upgrade -n tmp --install --set "echo.name"="world" echo2 ./echo
```