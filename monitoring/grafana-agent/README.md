# Grafana Agent

https://grafana.com/docs/agent/latest/flow/
https://github.com/grafana/agent/tree/main/operations/helm/charts/grafana-agent

```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

# Rabin
helm upgrade --install -n monitoring --values monitoring/grafana-agent/common.yaml --values monitoring/grafana-agent/values-dev.yaml grafana-agent grafana/grafana-agent  --version 0.38.0

# Golem
helm upgrade --install -n monitoring --values monitoring/grafana-agent/common.yaml --values monitoring/grafana-agent/values-prod.yaml grafana-agent grafana/grafana-agent  --version 0.38.0

```
