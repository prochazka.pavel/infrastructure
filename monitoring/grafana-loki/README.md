# Grafana Loki

https://grafana.com/docs/loki/latest/installation/helm/
https://github.com/grafana/loki/tree/main/production/helm/loki


```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
```

```bash
# Storage class
kaf monitoring/grafana-loki/storageClass.yaml

#Rabin
kaf ../rabin/grafana-loki/secret-azure.yaml 
helm upgrade --install -n monitoring --values monitoring/grafana-loki/values-dev.yaml loki grafana/loki --version 5.46.0

#Golem
kaf ../golem/grafana-loki/secret-azure.yaml 
helm upgrade --install -n monitoring --values monitoring/grafana-loki/values-prod.yaml loki grafana/loki --version 5.46.0   


```

# LogCLI
## Cardinality
Zjištění kardinality labels viz https://grafana.com/docs/loki/latest/best-practices/#be-aware-of-dynamic-labels-applied-by-clients
```bash
logcli series '{container_name=~".+"}' --analyze-labels
```
