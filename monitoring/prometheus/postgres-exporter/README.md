# Postgres exporter
https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus-postgres-exporter
https://github.com/prometheus-community/postgres_exporter


prometheus-postgres-exporter - PSQL metriky (SELECT ...) v 5m intervalu z psql-dp-(rabin|golem).
prometheus-postgres-exporter-vp - PSQL metriky (SELECT ...) v 1m intervalu z vehicle-positions DB.
```
# Rabin
helm upgrade --install -n monitoring monitoring-secret monitoring-secret --values .../rabin/monitoring/values-secret.yaml

#helm upgrade --install -n monitoring prometheus-postgres-exporter prometheus-community/prometheus-postgres-exporter --values monitoring/prometheus/postgres-exporter/values-dev.yaml --version 4.4.0 
helm upgrade --install -n monitoring prometheus-postgres-exporter-vp-frequently prometheus-community/prometheus-postgres-exporter --values monitoring/prometheus/postgres-exporter/values-vp-frequently-dev.yaml --version 4.4.0

# Golem
helm upgrade --install -n monitoring monitoring-secret monitoring-secret --values ../golem/monitoring/values-secret.yaml

helm upgrade --install -n monitoring prometheus-postgres-exporter-dp            prometheus-community/prometheus-postgres-exporter --values monitoring/prometheus/postgres-exporter/values-dp-prod.yaml --version 4.4.0    
helm upgrade --install -n monitoring prometheus-postgres-exporter-vp            prometheus-community/prometheus-postgres-exporter --values monitoring/prometheus/postgres-exporter/values-vp-prod.yaml --version 4.4.0
helm upgrade --install -n monitoring prometheus-postgres-exporter-vp-frequently prometheus-community/prometheus-postgres-exporter --values monitoring/prometheus/postgres-exporter/values-vp-frequently-prod.yaml --version 4.4.0 
```

Jen produkce:

prometheus-postgres-exporter-vp-15m - PSQL metriky (SELECT ...) v 5m intervalu z psql-vp-golem.
prometheus-redis-exporter - metriky z Redisu pro ROPID Grafana dashboard.
```
helm upgrade --install -n monitoring prometheus-postgres-exporter-vp-15m prometheus-community/prometheus-postgres-exporter --values monitoring/prometheus/postgres-exporter/vp-values-15m-prod.yaml  \
 --version 4.2.1 
```