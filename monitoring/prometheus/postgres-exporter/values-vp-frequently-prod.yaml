resources:
  limits:
    cpu: 20m
    memory: 30Mi
  requests:
    cpu: 10m
    memory: 15Mi
rbac:
  create: true
  pspEnabled: false
config:
  datasource:
    host: psql-vp2-golem.postgres.database.azure.com
    user: prometheus
    passwordSecret:
      name: monitoring-secret
      key: postgres-exporter-vp-ds-password
    database: vehicle-positions
    port: "6432"
    sslmode: verify-full
  logLevel: warn
  disableCollectorBgwriter: true
  disableDefaultMetrics: true
  disableSettingsMetrics: true
  autoDiscoverDatabases: false
  queries: |
    vp_last_position_trams:
      query: |
        SELECT count,
          CASE WHEN (extract(epoch FROM CURRENT_TIMESTAMP::time) <  1800
                 OR  extract(epoch FROM CURRENT_TIMESTAMP::time) > 16200)
                AND count < 10
                 OR (extract(epoch FROM CURRENT_TIMESTAMP::time) >=  1800
                AND  extract(epoch FROM CURRENT_TIMESTAMP::time) <= 16200)
                AND count < 0
               THEN 1 ELSE 0 END AS alert
        FROM (SELECT count(gtfs_route_short_name) AS count
              FROM pid.vehiclepositions_trips t1
              LEFT JOIN pid.vehiclepositions_positions t2 ON t1.last_position_id = t2.id
              WHERE t1.gtfs_route_type = 0
                AND t2.updated_at >= CURRENT_TIMESTAMP - INTERVAL '5 minutes') AS t3
      metrics:
        - count:
            usage: GAUGE
            description: Number of trams having their position updated in the last 5 minutes.
        - alert:
            usage: GAUGE
            description: Value (1/0) indicating whether an alert should be sent.

    vp_last_position_dpp_buses:
      query: |
        SELECT count,
          CASE WHEN (extract(epoch FROM CURRENT_TIMESTAMP::time) <  1800
                 OR  extract(epoch FROM CURRENT_TIMESTAMP::time) > 16200)
                AND count < 40
                 OR (extract(epoch FROM CURRENT_TIMESTAMP::time) >=  1800
                AND  extract(epoch FROM CURRENT_TIMESTAMP::time) <= 16200)
                AND count < 0
               THEN 1 ELSE 0 END AS alert
        FROM (SELECT count(gtfs_route_short_name) AS count
              FROM pid.vehiclepositions_trips t1
              LEFT JOIN pid.vehiclepositions_positions t2 ON t1.last_position_id = t2.id
              WHERE t1.gtfs_route_type = 3
                AND t1.agency_name_real = 'DP PRAHA'
                AND t2.updated_at >= CURRENT_TIMESTAMP - INTERVAL '5 minutes') AS t3
      metrics:
        - count:
            usage: GAUGE
            description: Number of DPP buses having their position updated in the last 5 minutes.
        - alert:
            usage: GAUGE
            description: Value (1/0) indicating whether an alert should be sent.

    vp_last_position_other_buses:
      query: |
        SELECT count,
          CASE WHEN (extract(epoch FROM CURRENT_TIMESTAMP::time) <  1800
                 OR  extract(epoch FROM CURRENT_TIMESTAMP::time) > 16200)
                AND count < 50
                 OR (extract(epoch FROM CURRENT_TIMESTAMP::time) >=  1800
                AND  extract(epoch FROM CURRENT_TIMESTAMP::time) <= 16200)
                AND count < 10
               THEN 1 ELSE 0 END AS alert
        FROM (SELECT count(gtfs_route_short_name) AS count
              FROM pid.vehiclepositions_trips t1
              LEFT JOIN pid.vehiclepositions_positions t2 ON t1.last_position_id = t2.id
              WHERE t1.gtfs_route_type = 3
                AND t1.agency_name_real != 'DP PRAHA'
                AND t2.updated_at >= CURRENT_TIMESTAMP - INTERVAL '5 minutes') AS t3
      metrics:
        - count:
            usage: GAUGE
            description: Number of non-DPP buses having their position updated in the last 5 minutes.
        - alert:
            usage: GAUGE
            description: Value (1/0) indicating whether an alert should be sent.

    vp_last_position_trains:
      query: |
        SELECT count,
          CASE WHEN (extract(epoch FROM CURRENT_TIMESTAMP::time) <  3600
                 OR  extract(epoch FROM CURRENT_TIMESTAMP::time) > 14400)
                AND count < 5
                 OR (extract(epoch FROM CURRENT_TIMESTAMP::time) >=  3600
                AND  extract(epoch FROM CURRENT_TIMESTAMP::time) <= 14400)
                AND count < 0
               THEN 1 ELSE 0 END AS alert
        FROM (SELECT count(gtfs_route_short_name) AS count
              FROM pid.vehiclepositions_trips t1
              LEFT JOIN pid.vehiclepositions_positions t2 ON t1.last_position_id = t2.id
              WHERE t1.gtfs_route_type = 2
                AND t2.updated_at >= CURRENT_TIMESTAMP - INTERVAL '5 minutes') AS t3
      metrics:
        - count:
            usage: GAUGE
            description: Number of trains having their position updated in the last 5 minutes.
        - alert:
            usage: GAUGE
            description: Value (1/0) indicating whether an alert should be sent.

    vp_last_position_metro:
      query: |
        SELECT count,
          CASE WHEN (extract(epoch FROM CURRENT_TIMESTAMP::time) > 18000)
                AND count < 5
                 OR (extract(epoch FROM CURRENT_TIMESTAMP::time) <= 18000)
                AND count < 0
               THEN 1 ELSE 0 END AS alert
        FROM (SELECT count(gtfs_route_short_name) AS count
              FROM pid.vehiclepositions_trips t1
              LEFT JOIN pid.vehiclepositions_positions t2 ON t1.last_position_id = t2.id
              WHERE t1.gtfs_route_type = 1
                AND t2.updated_at >= CURRENT_TIMESTAMP - INTERVAL '5 minutes') AS t3
      metrics:
        - count:
            usage: GAUGE
            description: Number of metro trips having their position updated in the last 5 minutes.
        - alert:
            usage: GAUGE
            description: Value (1/0) indicating whether an alert should be sent.

    vp_last_position_delay:
      query: |
        SELECT MIN(delay) AS minimum, MAX(delay) AS maximum,
          (
            SELECT COUNT(*)
            FROM pid.v_vehiclepositions_alerts_last_position
            WHERE updated_at >= CURRENT_TIMESTAMP - INTERVAL '1 minute'
              AND delay >= -100
              AND delay <= 1000
          ) AS in_range, COUNT(*) AS total_non_null
        FROM pid.v_vehiclepositions_alerts_last_position
        WHERE updated_at >= CURRENT_TIMESTAMP - INTERVAL '1 minute'
          AND delay IS NOT NULL
      metrics:
        - minimum:
            usage: GAUGE
            description: Minimal value of delay in the last minute.
        - maximum:
            usage: GAUGE
            description: Maximal value of delay in the last minute.
        - in_range:
            usage: GAUGE
            description: Number of records having delay within <-100, 1000> interval in the last minute.
        - total_non_null:
            usage: GAUGE
            description: Total number of records having non-null delay in the last minute.

    vp_last_position_ptime:
      query: |
        SELECT (
          SELECT COUNT(*)
          FROM pid.v_vehiclepositions_alerts_last_position
          WHERE
            updated_at >= CURRENT_TIMESTAMP - INTERVAL '1 minute'
            AND extract(epoch FROM updated_at - created_at) <= 15
               ) AS le15, COUNT(*) AS count
        FROM pid.v_vehiclepositions_alerts_last_position
        WHERE updated_at >= CURRENT_TIMESTAMP - INTERVAL '1 minute'
      metrics:
        - le15:
            usage: GAUGE
            description: "Number of processing times <= 15 seconds in the last minute."
        - count:
            usage: GAUGE
            description: Number of processed records in the last minute.

    vp_time:
      query: |
        SELECT extract(epoch FROM CURRENT_TIMESTAMP::time) AS seconds_of_day
      metrics:
        - seconds_of_day:
            usage: COUNTER
            description: Number of seconds since midnight of current timestamp.

    vp_ie_connection_pool:
      query: |
        SELECT application_name AS client, count(*) AS cnt
        FROM pg_stat_activity
        WHERE datname = 'vehicle-positions' AND application_name !=''
        GROUP BY application_name
      metrics:
        - client:
            usage: LABEL
            description: Pod name.
        - cnt:
            usage: GAUGE
            description: Number of connections from a specific pod.
