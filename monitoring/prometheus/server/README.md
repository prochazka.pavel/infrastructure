# Prometheus server
https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

# create storage class
kaf monitoring/prometheus/storageClass.yaml
```

# Dev
```
# helm uninstall -n monitoring prometheus
kubectl apply -f monitoring/prometheus/server/pvc-dev.yaml

helm upgrade --install -n monitoring prometheus prometheus-community/prometheus --values monitoring/prometheus/server/values-dev.yaml \
  --version ^22.6
```

# Prod
```
kubectl apply -f prometheus/prometheus-server-pvc-prod.yaml

helm upgrade --install -n monitoring prometheus prometheus-community/prometheus --values monitoring/prometheus/server/values-prod.yaml\
  --version ^22.6
```


# Examples

## TSDB Top 10 Metrics
```bash
topk(3, count by (__name__)({__name__=~".+"}))
```
