# SQL Exporter

Instal helm repo
```bash
helm repo add sql_exporter https://burningalchemist.github.io/sql_exporter/
```

# Configuration help
[deafult values](https://github.com/burningalchemist/sql_exporter/blob/master/helm/values.yaml)  
[source readme](https://github.com/burningalchemist/sql_exporter/tree/master/helm)  

# Rabin
```bash
helm upgrade -n monitoring --values ../rabin/sql-exporter/values-secret.yaml --values cluster_development/monitoring_sql-exporter.yaml sql-explorer sql_exporter/sql-exporter
```

# Golem
```bash
helm upgrade -n monitoring --values ../golemio/sql-exporter/values-secret.yaml --values cluster_production/monitoring_sql-exporter.yaml sql-explorer sql_exporter/sql-exporter
```
