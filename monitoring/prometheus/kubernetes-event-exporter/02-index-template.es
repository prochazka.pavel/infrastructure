PUT _ilm/policy/kube-events
{
  "policy": {
    "phases": {
      "hot": {
        "min_age": "0ms",
        "actions": {
          "rollover": {
            "max_primary_shard_size": "2gb",
            "max_age": "3d"
          },
          "set_priority": {
            "priority": 100
          }
        }
      },
      "delete": {
        "min_age": "3d",
        "actions": {
          "delete": {
            "delete_searchable_snapshot": true
          }
        }
      }
    }
  }
}

PUT _index_template/kube-events
{
  "template": {
    "settings": {
      "index": {
        "lifecycle": {
          "name": "kube-events"
        },
        "routing": {
          "allocation": {
            "include": {
              "_tier_preference": "data_content"
            }
          }
        }
      }
    },
    "mappings": {
      "dynamic": "true",
      "_size": {
        "enabled": true
      },
      "dynamic_date_formats": [
        "strict_date_optional_time",
        "yyyy/MM/dd HH:mm:ss Z||yyyy/MM/dd Z"
      ],
      "dynamic_templates": [],
      "date_detection": true,
      "numeric_detection": false,
      "properties": {
        "involvedObject": {
          "properties": {
            "labels": {
              "properties": {
                "app": {
                  "type": "text",
                  "index_options": "docs"
                }
              }
            }
          }
        }
      }
    },
    "aliases": {}
  },
  "index_patterns": [
    "kube-events-*"
  ]
}
