# Prometheus

## Blackbox exporter
[Blackbox exporter](blackbox-exporter)

## Elasticsearch exporter
https://github.com/prometheus-community/elasticsearch_exporter#metrics
https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus-elasticsearch-exporter
Installation:
```bash
helm upgrade --install -n monitoring prometheus-elasticsearch-exporter prometheus-community/prometheus-elasticsearch-exporter --values monitoring/prometheus/elasticsearch-exporter-values.yaml
```

## Kubernetes event exporter
[Kubernetes event exporter](kubernetes-event-exporter)

## Postgres exporter
[Postgres exporter](postgres-exporter)

## Redis exporter
[Redis exporter](redis)

## Prometheus server
[Prometheus server](server)


# Tutorials

https://satyanash.net/software/2021/01/04/understanding-prometheus-range-vectors.html
