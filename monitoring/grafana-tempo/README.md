# Grafana Tempo

https://github.com/grafana/helm-charts/tree/main/charts/tempo

# Tempo remotewrite
https://learn.microsoft.com/en-us/azure/azure-monitor/containers/prometheus-remote-write-managed-identity
Docker image also in link

```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

# Storage class
kaf monitoring/grafana-tempo/storageClass.yaml

# Rabin
helm upgrade --install -n monitoring --values monitoring/grafana-tempo/values-dev.yaml tempo grafana/tempo --version 1.7.2
kubectl apply -f monitoring/grafana-tempo/tempo-remotewrite.yaml

# Golem
helm upgrade --install -n monitoring --values monitoring/grafana-tempo/values-prod.yaml tempo grafana/tempo --version 1.7.2
kubectl apply -f monitoring/grafana-tempo/tempo-remotewrite.yaml
```

# Implementační poznámky
- nepodařilo se mi pouzit `backend: azure` ani s STORAGE_ACCOUNT_KEY ani jako user-assigned managed identity, tak je použit lokální disk viz https://grafana.com/docs/tempo/latest/configuration/azure/
