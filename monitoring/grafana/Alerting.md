# Alerting

## Notification templating
- [Základní info](https://grafana.com/docs/grafana/latest/alerting/fundamentals/alert-rules/message-templating/)
- [Ukázka výchozího kódu](https://github.com/grafana/alerting/blob/main/templates/default_template.go)
- [Templatovací jazyk](https://grafana.com/docs/grafana/latest/alerting/manage-notifications/template-notifications/using-go-templating-language/)
- [Dostupné proměnné](https://grafana.com/docs/grafana/latest/alerting/manage-notifications/template-notifications/reference/)

Pro testování je potreba si [nastavit opakované hlášení](https://grafana.com/docs/grafana/latest/alerting/fundamentals/notification-policies/notifications/#repeat-interval)
a krátkou dobu kontroly (`For`).

## Examples

Slack title
```
{{ .Status | toUpper }} | {{ index .CommonLabels "alertname" }}
```

Slack body - print label class_name and value
```
{{ define "__text_class_name" }}{{ range . }}
{{ index .Labels "class_name" }}: {{ index .Values "B" }}
{{/*
===
All debug: {{ . }}
*/}}
{{ end }}
{{ end }}
{{/* END of defined functions */}}

{{ template "__text_class_name" .Alerts.Firing }}
```