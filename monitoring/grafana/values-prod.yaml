ingress:
  enabled: true
  ingressClassName: nginx
  annotations:
    acme.cert-manager.io/http01-edit-in-place: "true"
    cert-manager.io/cluster-issuer: letsencrypt-prod
  hosts:
    - grafana.golemio.cz
  tls:
    - hosts:
        - grafana.golemio.cz
      secretName: grafana.golemio.cz-tls
rbac:
  pspEnabled: false
resources:
  limits:
    cpu: 200m
    memory: 384Mi
  requests:
    cpu: 50m
    memory: 150Mi
admin:
  existingSecret: monitoring-secret
  passwordKey: grafana-admin-password
  userKey: grafana-admin-user
env:
  GF_DATABASE_TYPE: postgres
  GF_LOG_LEVEL: WARN
  GF_SMTP_ENABLED: "true"
  GF_SERVER_DOMAIN: grafana.golemio.cz
  GF_SERVER_ENABLE_GZIP: "true"
  GF_SERVER_ROOT_URL: "https://grafana.golemio.cz"
  GF_DATE_FORMATS_INTERVAL_HOUR: "DD.MM. HH:mm"
  GF_DATE_FORMATS_INTERVAL_DAY: "DD.MM."
  GF_AUTH_AZURE_AUTH_ENABLED: true
  GF_AZURE_MANAGED_IDENTITY_ENABLED: true
envValueFrom:
  GF_DATABASE_HOST:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-postgres-host
  GF_DATABASE_NAME:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-postgres-db
  GF_DATABASE_USER:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-postgres-username
  GF_DATABASE_PASSWORD:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-postgres-password
  GF_SMTP_HOST:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-smtp-endpoint
  GF_SMTP_USER:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-smtp-user
  GF_SMTP_PASSWORD:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-smtp-password
  GF_SMTP_FROM_ADDRESS:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-smtp-from-address
  GF_SMTP_FROM_NAME:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-smtp-from-name
  GF_AUTH_AZUREAD_CLIENT_SECRET:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-azure-client-secret
  # Datasources
  PROMETHEUS_AZURE_SECRET:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-ds-prometheus-azure-secret
  PSQL_DATAPLATFORM_PASSWORD:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-ds-psql-dataplatform-password
  PSQL_VEHICLEPOSITIONS_PASSWORD:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-ds-psql-vehiclepositions-password
  AZURE_MONITOR_SECRET:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-ds-azure-monitor-secret
  ZABBIX_C4C_PASSWORD:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-ds-zabbix-c4c-password
  ZABBIX_CESNET_TOKEN:
    secretKeyRef:
      name: monitoring-secret
      key: grafana-ds-zabbix-cesnet-token

grafana.ini:
  auth.azuread:
    name: Azure AD
    enabled: true
    allow_sign_up: true
    auto_login: false
    client_id: a5bcb7fd-c650-4913-86fb-362f445da272
    scopes: openid email profile
    auth_url: https://login.microsoftonline.com/be28f71b-390e-4d7d-a948-94dc7db02ae3/oauth2/v2.0/authorize
    token_url: https://login.microsoftonline.com/be28f71b-390e-4d7d-a948-94dc7db02ae3/oauth2/v2.0/token
    allowed_domains:
    allowed_groups:
    allowed_organizations: be28f71b-390e-4d7d-a948-94dc7db02ae3
    role_attribute_strict: false
    allow_assign_grafana_admin: false
    skip_org_role_sync: false
    use_pkce: true
  database:
    ssl_mode: verify-full
  users:
    viewers_can_edit: true
plugins:
  - alexanderzobnin-zabbix-app

datasources:
  datasources.yaml:
    apiVersion: 1
    datasources:
      - name: Prometheus Azure monitor
        uid: prometheus-azure
        type: prometheus
        url: https://am-msprom-dp-golem-874p.westeurope.prometheus.monitor.azure.com
        access: proxy
        isDefault: true
        jsonData:
          manageAlerts: false
          prometheusType: Prometheus
          prometheusVersion: 2.40.1
          azureCredentials:
            authType: clientsecret
            azureCloud: AzureCloud
            clientId: b1657dae-028c-4749-8e32-c61a401dcc83
            tenantId: be28f71b-390e-4d7d-a948-94dc7db02ae3
        secureJsonData:
          azureClientSecret: $PROMETHEUS_AZURE_SECRET
      - name: PSQL Dataplatform
        uid: psql-dataplatform
        type: postgres
        url: psql-dp2-golem.postgres.database.azure.com:6432
        user: grafana
        access: proxy
        jsonData:
          database: dataplatform
          maxOpenConns: 10
          connMaxLifetime: 2000 #https://community.grafana.com/t/connection-timeout-error-from-grafana-to-postgres/11157
          postgresVersion: 1400
          sslmode: verify-full
          tlsAuth: true
          tlsAuthWithCACert: true
          tlsConfigurationMethod: file-path
          tlsSkipVerify: false
        secureJsonData:
          password: $PSQL_DATAPLATFORM_PASSWORD
      - name: PSQL Vehicle positions
        uid: psql-vehiclepositions
        type: postgres
        url: psql-vp2-golem.postgres.database.azure.com:6432
        user: grafana
        access: proxy
        jsonData:
          database: vehicle-positions
          maxOpenConns: 10
          postgresVersion: 1500
          sslmode: verify-full
          tlsAuth: true
          tlsAuthWithCACert: true
          tlsConfigurationMethod: file-path
          tlsSkipVerify: false
        secureJsonData:
          password: $PSQL_VEHICLEPOSITIONS_PASSWORD
      - name: Azure monitor
        uid: azure-monitor
        type: grafana-azure-monitor-datasource
        jsonData:
          azureAuthType: clientsecret
          clientId: b1657dae-028c-4749-8e32-c61a401dcc83
          cloudName: azuremonitor
          subscriptionId: 69c4f8e4-d7c8-446a-be3f-42d11543d311
          tenantId: be28f71b-390e-4d7d-a948-94dc7db02ae3
        secureJsonData:
          clientSecret: $AZURE_MONITOR_SECRET
      - name: Loki
        uid: loki
        type: loki
        url: http://loki.monitoring.svc.cluster.local:3100
      - name: Tempo
        uid: tempo
        type: tempo
        url: http://tempo.monitoring.svc.cluster.local:3100
        jsonData:
          lokiSearch:
            datasourceUid: loki
          nodeGraph:
            enabled:true
          serviceMap:
            datasourceUid: prometheus-azure
          spanBar:
            type: Duration
          tracesToLogsV2:
            customQuery: false
            datasourceUid: tempo
      - name: Zabbix Cesnet
        uid: zabbix-cesnet
        type: alexanderzobnin-zabbix-datasource
        url: https://zbx2cesnet.ict-operator.cz/api_jsonrpc.php
        jsonData:
          cacheTTL: ""
          disableDataAlignment: false
          authType: token
          trends: true
          trendsFrom: ""
          trendsRange: ""
        secureJsonData:
          apiToken: $ZABBIX_CESNET_TOKEN
