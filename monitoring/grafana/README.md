# Grafana

https://github.com/grafana/helm-charts/tree/main/charts/grafana

```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

# Rabin
helm upgrade --install -n monitoring --values monitoring/grafana/values-dev.yaml  grafana grafana/grafana --version 7.3.11

# Golem
helm upgrade --install -n monitoring --values monitoring/grafana/values-prod.yaml grafana grafana/grafana --version 7.3.11
```


# Message templates
https://grafana.com/docs/grafana/latest/alerting/manage-notifications/template-notifications/reference/
https://grafana.com/docs/grafana/latest/alerting/manage-notifications/template-notifications/using-go-templating-language/
https://github.com/grafana/alerting/blob/main/templates/default_template.go