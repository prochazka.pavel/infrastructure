# ELK stack logging

```
kubectl create namespace monitoring
```

## Elastic Cloud on Kubernetes (ECK)

https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-install-helm.html

Elastic je nainstalován do kubernetu pomocí Kubernetes operátora. 3 nodový ES cluster.

Install [custom resource definitions](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/) and the operator with its RBAC rules:

Postup dle (https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-install-helm.html):

https://www.elastic.co/guide/en/cloud-on-k8s/2.6/k8s-operator-config.html

```
helm repo add elastic https://helm.elastic.co
helm repo update

helm show values elastic/eck-operator

# Instal
helm upgrade --install elastic-operator elastic/eck-operator \
    -n monitoring \
    --values monitoring/elk/eck-operator-values.yaml
    
# Upgrade
helm upgrade elastic-operator elastic/eck-operator -n monitoring
```

Instalalace proběhne do namespace `monitoring`

Monitor the operator logs:
```
kubectl -n monitoring get all
kubectl -n monitoring logs -f statefulset.apps/elastic-operator
```

### Elasticsearch Cluster

Installation:

```bash
# create storage class
kaf monitoring/elk/storageClass.yaml

# (Optional) nastavení hesla do elasticu
kubectl create secret generic elasticsearch-es-elastic-user --from-literal=elastic=CHANGEME267

# Rabin
kubectl apply -f monitoring/elk/elasticsearch-cluster-dev.yaml

# Golem
kubectl apply -f monitoring/elk/elasticsearch-cluster-prod.yaml
```

počkat na `HEALTH` status `green`:

```
kubectl get elasticsearch

NAME         HEALTH   NODES   VERSION   PHASE   AGE
quickstart   green    3       7.10.0    Ready   17m
```

A default user named `elastic` is automatically created with the password stored in a Kubernetes secret:
```
kubectl get secret elasticsearch-es-elastic-user  -o go-template='{{.data.elastic | base64decode}}'
PASSWORD=$(kubectl get secret elasticsearch-es-elastic-user  -o go-template='{{.data.elastic | base64decode}}'')
```

Elasticsearch verze 8 používá Elastic Common Schema (ECS) jako výchozí, tzn.
očekává od Logstashe záznamy v logu ve struktuře ECS (tj. např. pole host
už není jen řetězec, ale objekt).
https://www.elastic.co/guide/en/ecs/current/ecs-host.html

### Kibana
```bash
# Rabin
kubectl apply -f monitoring/elk/kibana-dev.yaml

# Golem
kubectl apply -f monitoring/elk/kibana-prod.yaml

# Get password for user elastic
kubectl get secret elasticsearch-es-elastic-user  -o go-template='{{.data.elastic | base64decode}}'
```

## Logstash

https://github.com/elastic/helm-charts/tree/main/logstash

```bash
helm repo add elastic https://helm.elastic.co
```

```bash
helm upgrade --install --namespace monitoring --values monitoring/elk/logstash-values.yaml logstash elastic/logstash --version=^8.5
```




## ELK stack konfigurace

Kibana - create an index pattern

https://127.0.0.1:5601/app/kibana#/management/kibana/index_pattern?_g=()

Index pattern name: `logs*`

“Next Step”, select `@timestamp` as the Time filter field name and click on "Create index pattern"

## APM server

deploy an APM Server:

```
kubectl apply -f apm-server.yaml

kubectl get apmservers
```

Na APM serveru je nastaveno

- enabled SSL
- anonymous přístup (jaeger agent = oltp)

Naopak SSL přístup z APM do kibany a na elasticsearch je v konfiguraci vypnutý:

```
protocol: "http"
ssl:
  enabled: false
```

## Tipy a triky

### Jaeger nezapisuje do APM

Pokud se v logu začnou objevovat záznamy jako
```
...resource 'apm-7.15.0-span' exists, but it is not an alias...
```
a v menu Kibany `Dev Tools` příkaz
```
GET _cat/indices?v&s=index
```
vypíše
```
apm-7.15.0-span
```
místo
```
apm-7.15.0-span-000001
```
, je potřeba provést následující kroky:

1. Zakázat zápis
```
PUT apm-7.15.0-span/_settings
{
  "settings": {
    "index.blocks.write": true
  }
}
```
2. Odkopírovat stará data (pozor na místo na disku!)
```
POST apm-7.15.0-span/_clone/apm-7.15.0-span-original
```
3. Počkat, dokud se předchozí operace nedokončí (vše ve stavu `done`)
```
GET _cat/recovery/apm*span*?s=index&v=true&h=index,stage
```
4. Smazat původní index
```
DELETE apm-7.15.0-span
```
5. Počkat, až se vytvoří nový
```
GET _cat/aliases/apm*span*?s=index&v=true&h=alias,index,is_write_index
```
6. Nakopírovat data ze starého indexu do nového (pozor na místo na disku!)
```
POST _reindex
{
  "source": {
    "index": "apm-7.15.0-span-original"
  },
  "dest": {
    "index": "apm-7.15.0-span"
  }
}
```
7. Počkat, dokud se data nezkopírují
```
GET _tasks?detailed=true&actions=*reindex&group_by=none
```
8. Smazat starý index
```
DELETE apm-7.15.0-span-original
```
Obvykle takto vypadnou `apm-7.15.0-metric`, `apm-7.15.0-span` a `apm-7.15.0-transaction`.

### Změna index template (mapping)

Změnit mapping u existujícího indexu lze pouze v případě, kdy přidáváte nějaké pole.
Jinak (při změně, mazání) je nutné založit index nový a data ze starého do něj překopírovat pomocí reindexace.

#### Postup
1. Upravit šablonu a přidat mezi index patterns další (nové) index(y)

Příklad:

Původně:
```
  "index_patterns": [
    "logs-postgres-*"
  ],
```
Nově:
```
  "index_patterns": [
    "logs-postgres-*",
    "logs-postgres2-*"
  ],
```
2. Aktualizovat šablonu v Elasticsearch

Příklad:
```
PUT _index_template/logs-postgres-14days
{
  ...
  ...
```
3. Přesměrovat posílání logů do nového indexu/data streamu

Tzn. úprava konfigurace Logstashe a redeploy. Do nového indexu/data streamu se bude zapisovat již podle upravené šablony.

4. Zkopírovat data ze starého indexu/data streamu do nového pomocí reindexace

Příklad:
```
POST /_reindex
{
  "source": {
    "index": ".ds-logs-postgres-14days-2099.03.07-000001"
  },
  "dest": {
    "index": "logs-postgres2-14days",
    "op_type": "create"
  }
}
```
POZOR! dest.index není v případě použití data streamu jméno indexu, ale celého data streamu!
Pokud je třeba kopírovat data za vícero dní, opakovat s dalšími source.index.

5. Smazat starý index/data stream

Příklad:
```
DELETE /_data_stream/logs-postgres-14days
```
A protože na původní data stream máme určitě nějaké index patterns v Kibaně, uděláme to ještě jednou do původního názvu indexu/data streamu.

6. Přesměrovat posílání logů do původního (smazaného) indexu/data streamu

Tzn. úprava konfigurace Logstashe zpět a redeploy.

7. Zkopírovat data z nového indexu/data streamu do původního pomocí reindexace

Příklad:
```
POST /_reindex
{
  "source": {
    "index": ".ds-logs-postgres2-14days-2099.03.07-000001"
  },
  "dest": {
    "index": "logs-postgres-14days",
    "op_type": "create"
  }
}
```
8. Smazat nový index/data stream

Příklad:
```
DELETE /_data_stream/logs-postgres2-14days
```
9. (Volitelně) vrátit změny index patterns v šabloně.

Příklad:

Z
```
  "index_patterns": [
    "logs-postgres-*",
    "logs-postgres2-*"
  ],
```
na
```
  "index_patterns": [
    "logs-postgres-*"
  ],
```
a aplikovat do Elasticsearche via `PUT _index_template/logs-postgres-14days`

Voilà!
