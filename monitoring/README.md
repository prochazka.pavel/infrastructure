# Monitoring


## Azure - nastavení AKS
Nastaveni OMS agenta co má jak sbírat se dělá pomocí nasazení ConfigMap v container-azm-ms-agentconfig.yaml viz https://docs.microsoft.com/en-us/azure/azure-monitor/containers/container-insights-agent-config a na https://docs.microsoft.com/en-us/azure/azure-monitor/containers/container-insights-onboard

```bash
kaf monitoring/container-azm-ms-agentconfig.yaml
```
https://learn.microsoft.com/en-us/azure/azure-monitor/containers/container-insights-agent-config


## Logování
- [Elasticsearch, Logstash, Kibana (ELK)](elk/README.md)
- [Vector](../archive/vector/README.md)
- ~~[FluentBit](fluentbit/README.md)~~

## Grafana
[Grafana](grafana/README.md)

## Prometheus
[Prometheus](prometheus/README.md)

# Notes
- 15.2. 2023 - FluentBit a Logstash nahrazen Vectorem pro lepší efektivitu a jednotný nástroj pro obě fáze