global:
  imageRegistry: registry.gitlab.com/operator-ict/golemio
  imagePullSecrets: ["gitlab-registry"]

redis:
  enabled: true

opentelemetry:
  host: grafana-agent.monitoring.svc.cluster.local
  #host: tempo.observability.svc.cluster.local
  #host: jaeger-agent.observability.svc.cluster.local
  logLevel: 50 # 50=WARN 10=debug

sentry:
  enabled: false

loadBalancer:
  pipName: "aks-dp-rabin-public"
  resourceGroup: "rg-aks-dp-rabin"

input-gateway-v1:
  enabled: true
  name: input-gateway-v1-vp
  repository: code/vp/input-gateway/v1
  logLevel: WARN
  replicas: 2
  rabbit:
    exchangeName: vehicle-positions
  probes:
    enabled: true
  opentelemetry:
    enabled: true
    samplerArg: '0.1'
  metrics:
    enabled: true
  azure:
    enabled: true
    container: "rabin-input-gateway"
  resources:
    limits:
      cpu: 200m
      memory: 600Mi
    requests:
      cpu: 100m
      memory: 192Mi

tcp-input-gateway:
  enabled: true
  name: tcp-input-gateway-vp
  tram:
    port: 3000
  metro:
    port: 3007
  bus:
    port: 3002
  arriva:
    port: 3009
  opentelemetry:
    enabled: true
    samplerArg: '0.1'
  replicas: 2
  logLevel: WARN
  rabbit:
    exchangeName: vehicle-positions
  azure:
    enabled: true
    container: "rabin-input-gateway"
  azureTableStorage:
    enabled: true
    account: "rabingolemio"
    entityBatchSize: 100

integration-engine:
  enabled: true
  name: integration-engine-vp
  repository: code/vp/integration-engine
  rabbit:
    exchangeName: vehicle-positions
    maxPrefetchCount: 1
  replicas: 10
  minReplicas: 10
  maxReplicas: 13
  logLevel: WARN
  nodeEnv: production
  azure:
    enabled: true
    container: "rabin-integration-engine"
  photonURL: https://photon-test.ipt.oict.cz/reverse
  opentelemetry:
    enabled: true
    samplerArg: '0.1'
  postgres:
    minConnections: 0
    maxConnections: 13
    idleTimeout: 60000
  resources:
    requests:
      cpu: 400m
      memory: 512Mi
    limits:
      cpu: 700m
      memory: 1500Mi
  probes:
    enabled: false
  metrics:
    enabled: true
  azureTableStorage:
    enabled: true
    account: "rabingolemio"
    entityBatchSize: 100
  vehiclePositions:
    dataMaintenanceEnabled: true
    redisExpireTime: "01:00"
    metroArrivalDelayInSeconds: 15
    metroDepartureDelayInSeconds: -5
    logBatchSize: 1000
    logLockTimeout: 60000

cron:
  enabled: true
  name: cron
  logLevel: WARN
  nodeEnv: "production"
  opentelemetry:
    enabled: true
    samplerArg: '0.1'

golemio-database-migration:
  enabled: true
  commands:
    - "-c"
    - "npx golemio migrate-db up --postgres"

output-gateway-v2:
  enabled: true
  name: output-gateway-v2-vp
  repository: code/vp/output-gateway/v2
  branch: development
  replicas: 1
  minReplicas: 1
  maxReplicas: 2
  logLevel: "WARN"
  opentelemetry:
    enabled: true
    samplerArg: '0.1'
  probes:
    enabled: true
  metrics:
    enabled: false
  postgres:
    minConnections: 1
    maxConnections: 5
    idleTimeout: 10000
  vehiclePositions:
    isAirCondEnabled: true
  resources:
    requests:
      cpu: 700m
      memory: 320Mi
    limits:
      cpu: 1300m
      memory: 768Mi
url-proxy:
  enabled: true
  name: url-proxy
  ingress:
    hosts: ["s.rabin.golemio.cz"]
    tls: true
  golemioApiV2Url: "http://access-proxy.permission-services:3000/v2"
  opentelemetry:
    enabled: true
    samplerArg: '0.1'
  resources:
    requests:
      cpu: 50m
      memory: 200Mi
    limits:
      cpu: 100m
      memory: 512Mi

configurationFiles:
  inputGwSaveRawData: |-
    [
        {
            "route": "/vehiclepositions",
            "withHeaders": false
        }
    ]
  integrationEngine:
    queueBlackList: |-
      {
      }

  cronTasks: |-
    [
      {
          "cronTime": "0 35 5 * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"forceRefresh\": true }",
          "routingKey": "cron.vehicle-positions.ropidgtfs.checkForNewData"
      },
      {
          "cronTime": "0 35 12 * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"forceRefresh\": false }",
          "routingKey": "cron.vehicle-positions.ropidgtfs.checkForNewData"
      },
      {
          "cronTime": "0 11 */1 * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"intervalFromHours\": 5, \"intervalToHours\": 7 }",
          "routingKey": "cron.vehicle-positions.ropidgtfs.refreshPublicGtfsDepartureCache"
      },
      {
          "cronTime": "45 2 * * * *",
          "exchange": "vehicle-positions",
          "message": "",
          "routingKey": "cron.vehicle-positions.ropidvymi.checkForNewEvents"
      },
      {
          "cronTime": "*/30 * * * * *",
          "exchange": "vehicle-positions",
          "message": "",
          "routingKey": "cron.vehicle-positions.vehiclepositionsgtfsrt.generateFiles"
      },
      {
          "cronTime": "0 15 * * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetHours\": 16, \"repoName\": \"PositionsRepository\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteData"
      },
      {
          "cronTime": "0 15 * * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetHours\": 16, \"repoName\": \"PositionsHistoryRepository\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteData"
      },
      {
          "cronTime": "0 15 * * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetHours\": 16, \"repoName\": \"TripsRepository\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteData"
      },
      {
          "cronTime": "0 15 * * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetHours\": 16, \"repoName\": \"TripsHistoryRepository\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteData"
      },
      {
          "cronTime": "0 15 * * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetHours\": 16, \"repoName\": \"CisStopRepository\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteData"
      },
      {
          "cronTime": "0 15 * * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetHours\": 16, \"repoName\": \"StopTimesHistoryRepository\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteData"
      },
      {
          "cronTime": "0 30 * * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetHours\": 16, \"repoName\": \"CommonRunsRepository\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteData"
      },
      {
          "cronTime": "0 30 * * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetHours\": 16, \"repoName\": \"MetroRunsMessagesRepository\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteData"
      },
      {
          "cronTime": "0 30 * * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetHours\": 16, \"repoName\": \"RegionalBusRunsMessagesRepository\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteData"
      },
      {
          "cronTime": "0 25 5 * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetHours\": 24, \"repoName\": \"DescriptorRepository\"  }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteData"
      },
      {
          "cronTime": "30 15,30 * * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetHours\": 8, \"repoName\": \"PresetLogRepository\"  }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteData"
      },
      {
          "cronTime": "0 18,48 * * * *",
          "exchange": "vehicle-positions",
          "message": "",
          "routingKey": "cron.vehicle-positions.vehiclepositions.dataRetention"
      },
      {
          "cronTime": "*/50 * * * * *",
          "exchange": "vehicle-positions",
          "message": "",
          "routingKey": "cron.vehicle-positions.vehiclepositions.refreshPublicTripCache"
      },
      {
          "cronTime": "*/50 * * * * *",
          "exchange": "vehicle-positions",
          "message": "",
          "routingKey": "cron.vehicle-positions.vehiclepositions.refreshPublicStopTimeCache"
      },
      {
          "cronTime": "0 15 5,12 * * *",
          "exchange": "vehicle-positions",
          "message": "",
          "routingKey": "cron.vehicle-positions.vehicledescriptors.refreshDescriptors"
      },
      {
          "cronTime": "0 */1 * * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"targetMinutes\": 3 }",
          "routingKey": "cron.vehicle-positions.ropidpresets.collectAndSaveLogs",
          "disabled": true,
          "reason": "Preset logy z Rabina ted ROPID nepotrebuje"
      },
      {
          "cronTime": "0 1 0,1,2 * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"tableName\": \"TcpDppTramData\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteMonthOldStorageData"
      },
      {
          "cronTime": "0 1 0,1,2 * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"tableName\": \"TcpDppBusData\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteMonthOldStorageData"
      },
      {
          "cronTime": "0 31 0,1,2 * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"tableName\": \"TcpDppMetroData\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteMonthOldStorageData"
      },
      {
          "cronTime": "0 31 0,1,2 * * *",
          "exchange": "vehicle-positions",
          "message": "{ \"tableName\": \"TcpArrivaCityData\" }",
          "routingKey": "cron.vehicle-positions.dataretention.deleteMonthOldStorageData"
      }
    ]
