# Backstage
https://backstage.io/docs/overview/what-is-backstage
https://github.com/backstage/charts/tree/main/charts/backstage

## Installation
```bash
helm repo add backstage https://backstage.github.io/charts
helm repo update
## DEV
helm upgrade --install -n backstage --values backstage/values-dev.yaml backstage backstage/backstage
```

# Upgrade
See https://backstage.io/docs/getting-started/keeping-backstage-updated#updating-backstage-versions-with-backstage-cli